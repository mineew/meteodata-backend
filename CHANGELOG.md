# 2.0.0 (Jan 1, 2019)

## 2.6.2 (Apr 9, 2019)

* Dependencies updated.

## 2.6.1 (Mar 25, 2019)

* Changed the label of the rain (as sum) in the exported files.

## 2.6.0 (Mar 24, 2019)

* Rain is considered as the sum, not the average.

## 2.5.0 (Mar 20, 2019)

* Export only one metrics.

## 2.4.1 (Mar 19, 2019)

* `aggregateMetrics` excludes nulls and zeros;
* db excludes files with tabs;
* Dependencies updated.

## 2.2.1 (Jan 18, 2019)

* Added `chartTable` query, `chart` query is deleted.

## 2.1.1 (Jan 13, 2019)

* Table can group metrics and add hours.

## 2.0.0 (Jan 1, 2019)

* Sunrise and sunset calculation based on lat and long.

# 1.0.0 (Dec 18, 2018)

## 1.4.0 (Dec 27, 2018)

* Added charts.

## 1.3.4 (Dec 24, 2018)

* Fix `aggregateMetrics` bug.

## 1.3.4 (Dec 23, 2018)

* Resolver throws errors;
* Added export for browser;
* Dependencies updated.

## 1.3.1 (Dec 21, 2018)

* More intuitive names in graphql schema;
* Added `export*` mutations.

## 1.1.0 (Dec 18, 2018)

* Added CORS support.

## 1.0.0 (Dec 18, 2018)

* MeteoData express (graphql) backend is released.
