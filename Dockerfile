FROM node:lts-alpine

WORKDIR /meteodata-backend

COPY package.json yarn.lock ./
RUN yarn

COPY . ./

CMD ["yarn", "pipeline"]
