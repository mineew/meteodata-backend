const pt = require('path')
const moment = require('moment')
const mockFs = require('mock-fs')

jest.mock('open', () => jest.fn())
const opn = require('open')

const excelModule = require('../db/excel')
excelModule.exportCalendarYear = jest.fn()
excelModule.exportCalendarMonth = jest.fn()
excelModule.exportTable = jest.fn()

const resolver = require('./resolver')

const fs = {
  '/some/root': {
    '20070311.DAT': `
00:00:00  1,7  0,0  3,3 156  64 263   5,1   5,0   5,1 97 97 97   5,0   5,0   5,0  987,0  987,0  987,0  23,1  23,1  23,1 43 43 43   0   4,2   1,0   5,0
00:10:00  2,1  0,0  3,3 152  52 204   5,1   5,0   5,1 97 97 97   5,0   5,0   5,0  987,0  987,0  987,0  23,1  23,0  23,1 43 43 43   0   3,5   1,0   5,0
00:20:00  1,9  0,0  3,5 142  11 190   5,1   5,0   5,1 97 97 97   5,0   5,0   5,0  986,1  986,0  987,0  23,0  23,0  23,0 43 43 43   0   4,1   1,0   5,0
`.split('\n').join('\r\n'),
    '20070312.DAT': `
07:20:00  6,4  4,0  9,0 163  99 187  -0,7  -0,8  -0,7 86 86 86  -2,8  -2,8  -2,7  991,0  991,0  991,0  21,1  21,1  21,1 28 28 28   0 -12,1 -16,0  -8,0
07:50:00  6,6  2,7  9,4 168 123 198  -0,8  -0,8  -0,7 86 86 86  -2,8  -2,8  -2,7  991,0  991,0  991,0  21,1  21,1  21,1 28 28 28   0 -12,4 -16,0  -4,0
`.split('\n').join('\r\n'),
    '20080722.DAT': 'content 3',
    '20051101.DAT': `
00:50:00  2,0  0,0  2,9 329   2 357  12,4  12,4  12,4 98 98 98  12,0  12,0  12,0 1004,0 1004,0 1004,0  21,7  21,7  21,7 53 53 53   0  11,6  10,0  12,0
01:00:00  2,0  1,4  3,3 332 291 357  12,4  12,3  12,4 98 98 98  12,0  12,0  12,0 1004,0 1004,0 1004,0  21,7  21,7  21,7 53 53 53   0  11,2   9,0  12,0
01:00:00  2,0  1,4  3,3 332 291 357  12,4  12,3  12,4 98 98 98  12,0  12,0  12,0 1004,0 1004,0 1004,0  21,7  21,7  21,7 53 53 53   0  11,2   9,0  12,0
01:10:00  1,6  0,0  2,5 332   2 357  12,3  12,3  12,4                            1004,0 1004,0 1004,0  21,7  21,7  21,7 53 53 53   0  11,9  10,0  12,0
01:20:00  1,4  0,0  2,3 344   0 357  12,4  12,4  12,5 98 98 98  12,1  12,0  13,0 1004,0 1004,0 1004,0  21,7  21,7  21,7 53 53 53   0  12,1  12,0  13,0
`.split('\n').join('\r\n'),
    'qwerty.DAT': 'content 5',
    '200511.DAT': 'content 6',
    '20051101 (2).DAT': `
00:00:00  1,7  0,0  3,3 156  64 263   5,1   5,0   5,1 97 97 97   5,0   5,0   5,0  987,0  987,0  987,0  23,1  23,1  23,1 43 43 43   0   4,2   1,0   5,0
00:10:00  2,1  0,0  3,3 152  52 204   5,1   5,0   5,1 97 97 97   5,0   5,0   5,0  987,0  987,0  987,0  23,1  23,0  23,1 43 43 43   0   3,5   1,0   5,0
00:20:00  1,9  0,0  3,5 142  11 190   5,1   5,0   5,1 97 97 97   5,0   5,0   5,0  986,1  986,0  987,0  23,0  23,0  23,0 43 43 43   0   4,1   1,0   5,0
`.split('\n').join('\r\n'),
    'file.txt': 'content 8',
    'subdir1': {
      'subdir2': {
        '20170915.DAT': 'content 9',
        '20170916.DAT': 'content 10',
        '20180701.DAT': 'content 11',
        '20051027.DAT': `
07:20:00  6,4  4,0  9,0 163  99 187  -0,7  -0,8  -0,7 86 86 86  -2,8  -2,8  -2,7  991,0  991,0  991,0  21,1  21,1  21,1 28 28 28   0 -12,1 -16,0  -8,0
07:50:00  6,6  2,7  9,4 168 123 198  -0,8  -0,8  -0,7 86 86 86  -2,8  -2,8  -2,7  991,0  991,0  991,0  21,1  21,1  21,1 28 28 28   0 -12,4 -16,0  -4,0
`.split('\n').join('\r\n'),
        'file.png': Buffer.from([1, 2, 3]),
        'subdir3': {}
      }
    }
  }
}

// Архангельск
const arhLat = 64.5
const arhLong = 40.5

beforeAll(() => {
  mockFs(fs)
})

afterAll(() => {
  mockFs.restore()
})

afterEach(() => {
  opn.mockClear()
  excelModule.exportCalendarYear.mockClear()
  excelModule.exportCalendarMonth.mockClear()
  excelModule.exportTable.mockClear()
})

describe('resolver', () => {
  it('accepts the `setRoot` mutation', async () => {
    const response = await resolver.setRoot({ root: '/some/root' })
    const invalids = response.invalidPaths.map((path) => pt.basename(path))
    invalids.sort()
    expect(response.filesCount).toBe(9)
    expect(invalids).toEqual(['200511.DAT', 'qwerty.DAT'])
    expect(response.years).toEqual([2005, 2007, 2008, 2017, 2018])
  })

  it('accepts the `loadDatabase` mutation', async () => {
    const response = await resolver.loadDatabase({ years: [2005, 2007] })
    expect(response.filesCount).toBe(5)
    expect(response.metricsCount).toBe(15 - 1)
    expect(response.missedMetricsCount).toBe(
      (365 + 365) * 144 - (15 - 1)
    )
    expect(response.brokenMetricsCount).toBe(1)
    expect(response.minDate).toBe(
      moment('27.10.2005 07:20', 'DD.MM.YYYY HH:mm').unix()
    )
    expect(response.maxDate).toBe(
      moment('12.03.2007 07:50', 'DD.MM.YYYY HH:mm').unix()
    )
  })

  it('accepts the `setLatLong` mutation', () => {
    const response = resolver.setLatLong({
      latitude: arhLat,
      longitude: arhLong
    })
    expect(response).toBe(true)
    expect(resolver.latitude).toBe(arhLat)
    expect(resolver.longitude).toBe(arhLong)
  })

  it('accepts the `exportCalendarYear` mutation', async () => {
    const opts = { year: 2007, path: '/some/path' }
    const response = await resolver.exportCalendarYear(opts)
    expect(excelModule.exportCalendarYear).toHaveBeenCalledTimes(1)
    expect(response).toBe(opts.path)
    await resolver.exportCalendarYear({ ...opts, open: true })
    expect(opn).toHaveBeenCalledTimes(1)
  })

  it('accepts the `exportCalendarMonth` mutation', async () => {
    const opts = { year: 2007, month: 2, path: '/some/path' }
    const response = await resolver.exportCalendarMonth(opts)
    expect(excelModule.exportCalendarMonth).toHaveBeenCalledTimes(1)
    expect(response).toBe(opts.path)
    await resolver.exportCalendarMonth({ ...opts, open: true })
    expect(opn).toHaveBeenCalledTimes(1)
  })

  it('accepts the `exportCalendar` mutation', async () => {
    const opts = { type: 'YEAR', year: 2007, month: 2, path: '/some/path' }
    const responseYear = await resolver.exportCalendar(opts)
    expect(excelModule.exportCalendarYear).toHaveBeenCalledTimes(1)
    expect(responseYear).toBe(opts.path)
    const responseMonth = await resolver.exportCalendar({
      ...opts,
      type: 'MONTH'
    })
    expect(excelModule.exportCalendarMonth).toHaveBeenCalledTimes(1)
    expect(responseMonth).toBe(opts.path)
    await resolver.exportCalendar({ ...opts, open: true })
    expect(opn).toHaveBeenCalledTimes(1)
  })

  it('accepts the `exportTable` mutation', async () => {
    const opts = {
      start: moment('11.03.2007', 'DD.MM.YYYY').unix(),
      end: moment('12.03.2007', 'DD.MM.YYYY').unix(),
      path: '/some/path'
    }
    const response = await resolver.exportTable(opts)
    expect(excelModule.exportTable).toHaveBeenCalledTimes(1)
    expect(response).toBe(opts.path)
    await resolver.exportTable({ ...opts, open: true })
    expect(opn).toHaveBeenCalledTimes(1)
    // can group
    excelModule.exportTable.mockClear()
    await resolver.exportTable({ ...opts, grouping: 2 })
    const tableGrouping = excelModule.exportTable.mock.calls[0][0]
    expect(tableGrouping[1].time).toBe('00:20')
    // can add hours
    excelModule.exportTable.mockClear()
    await resolver.exportTable({ ...opts, addHours: 3 })
    const tableAddHours = excelModule.exportTable.mock.calls[0][0]
    expect(tableAddHours[1].time).toBe('03:10')
  })

  it('responses on the `calendarYear` query', () => {
    const response = resolver.calendarYear({ year: 2007 })
    expect(response).toHaveLength(1)
    expect(response[0].month).toBe(2)
    expect(response[0].totalCount).toBe(5)
    expect(response[0].missedCount).toBe(31 * 144 - 5)
    expect(response[0].brokenCount).toBe(0)
    expect(Array.isArray(response[0].fullTime)).toBeTruthy()
    expect(Array.isArray(response[0].lightTime)).toBeTruthy()
    expect(Array.isArray(response[0].darkTime)).toBeTruthy()
  })

  it('responses on the `calendarMonth` query', () => {
    const response = resolver.calendarMonth({ year: 2007, month: 2 })
    expect(response).toHaveLength(2)
    expect(response[0].day).toBe(11)
    expect(response[0].totalCount).toBe(3)
    expect(response[0].missedCount).toBe(144 - 3)
    expect(response[0].brokenCount).toBe(0)
    expect(Array.isArray(response[0].fullTime)).toBeTruthy()
    expect(Array.isArray(response[0].lightTime)).toBeTruthy()
    expect(Array.isArray(response[0].darkTime)).toBeTruthy()
  })

  it('responses on the `calendar` query', () => {
    const yearResponse = resolver.calendar({
      type: 'YEAR', year: 2007
    })
    expect(yearResponse[0].month).toBe(2)
    expect(yearResponse[0].__typename).toBe('CalendarMonth')
    const monthResponse = resolver.calendar({
      type: 'MONTH', year: 2007, month: 2
    })
    expect(monthResponse[0].day).toBe(11)
    expect(monthResponse[0].__typename).toBe('CalendarDay')
  })

  it('responses on the `chartYear` query', () => {
    const response = resolver.chartYear({ year: 2007, metricsName: 'pressure' })
    expect(Array.isArray(response.fullTime)).toBeTruthy()
    expect(Array.isArray(response.lightTime)).toBeTruthy()
    expect(Array.isArray(response.darkTime)).toBeTruthy()
    expect(response.fullTime[2].name).toBe('март')
    expect(typeof response.fullTime[2].mean).toBe('number')
    expect(typeof response.fullTime[2].min).toBe('number')
    expect(typeof response.fullTime[2].max).toBe('number')
  })

  it('responses on the `chartMonth` query', () => {
    const response = resolver.chartMonth({
      year: 2007,
      month: 2,
      metricsName: 'pressure'
    })
    expect(Array.isArray(response.fullTime)).toBeTruthy()
    expect(Array.isArray(response.lightTime)).toBeTruthy()
    expect(Array.isArray(response.darkTime)).toBeTruthy()
    expect(response.fullTime[10].name).toBe('11')
    expect(typeof response.fullTime[10].mean).toBe('number')
    expect(typeof response.fullTime[10].min).toBe('number')
    expect(typeof response.fullTime[10].max).toBe('number')
  })

  it('responses on the `chartTable` query', () => {
    const start = moment('11.03.2007', 'DD.MM.YYYY').unix()
    const end = moment('12.03.2007', 'DD.MM.YYYY').unix()
    const response = resolver.chartTable({
      start,
      end,
      metricsName: 'pressure'
    })
    expect(Array.isArray(response)).toBeTruthy()
    expect(response[0].name).toBe('11 00:00')
    expect(typeof response[0].mean).toBe('number')
    expect(typeof response[0].min).toBe('number')
    expect(typeof response[0].max).toBe('number')
  })

  it('responses on the `table` query', () => {
    const start = moment('11.03.2007', 'DD.MM.YYYY').unix()
    const end = moment('12.03.2007', 'DD.MM.YYYY').unix()
    const response = resolver.table({ start, end })
    expect(response).toHaveLength(144 + 6 * 8)
    expect(response[3].date).toBe('11.03.2007')
    expect(response[3].time).toBe('00:30')
    expect(Array.isArray(response[3].data)).toBeTruthy()
    const responseWithGrouping = resolver.table({ start, end, grouping: 2 })
    expect(response[1].time).toBe('00:10')
    expect(responseWithGrouping[1].time).toBe('00:20')
    const responseWithAddHours = resolver.table({ start, end, addHours: 2 })
    expect(response[1].time).toBe('00:10')
    expect(responseWithAddHours[1].time).toBe('02:10')
  })
})

describe('`setRoot` mutation', () => {
  it('throws when there is no paths', async () => {
    const newResolver = require('./resolver')
    try {
      await newResolver.setRoot({ root: '/some/root/subdir1/subdir2/subdir3' })
    } catch (error) {
      expect(error).toBeDefined()
    }
  })
})

describe('`loadDatabase` mutation', () => {
  it('throws when there is no metrics', async () => {
    const newResolver = require('./resolver')
    await newResolver.setRoot({ root: '/some/root' })
    try {
      await newResolver.loadDatabase({ years: [2042] })
    } catch (error) {
      expect(error).toBeDefined()
    }
  })
})
