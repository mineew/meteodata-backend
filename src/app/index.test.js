const request = require('supertest')
const createApp = require('./index')

const PORT = 3001

describe('createApp', () => {
  it('creates an express app', async () => {
    const app = createApp()
    const server = app.listen(PORT)
    const response = await request(app).get('/')
    expect(response.status).toBe(404)
    server.close()
  })

  it('runs a graphql api server', async () => {
    const app = createApp()
    const server = app.listen(PORT)
    const response = await request(app).get('/graphql')
    const { errors } = response.body
    expect(errors[0].message).toMatch('Must provide query string')
    server.close()
  })

  it('can serve graphiql', async () => {
    const app = createApp({ graphiql: true })
    const server = app.listen(PORT)
    const response = await (
      request(app).get('/graphql').set('Accept', 'text/html')
    )
    expect(response.status).toBe(200)
    server.close()
  })

  it('can enable CORS', () => {
    const app = createApp({ corsOrigin: 'http://some.origin' })
    const hasCorsMiddleware = app._router.stack.some(
      ({ name }) => name === 'corsMiddleware'
    )
    expect(hasCorsMiddleware).toBeTruthy()
  })
})
