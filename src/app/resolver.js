const moment = require('moment')
const opn = require('open')
const {
  getFilePaths,
  selectValidPaths,
  selectInvalidPaths,
  getYearsFromPaths,
  filterPaths,
  readFiles,
  selectValidFiles
} = require('../db/fs')
const {
  createMetricsDatabase,
  sortMetrics,
  getMissedMetricsCount,
  getBrokenMetricsCount
} = require('../db/metrics')
const {
  getCalendarYear,
  getCalendarMonth
} = require('../db/calendar')
const {
  getChartYear,
  getChartMonth,
  getChartTable
} = require('../db/chart')
const {
  getTable
} = require('../db/table')
const {
  exportCalendarYear,
  exportCalendarMonth,
  exportTable
} = require('../db/excel')

/**
 * @typedef {import('../db/metrics').Metrics} Metrics
 */

const BULK_SIZE = 200
const FILES_NOT_FOUND_ERROR_MESSAGE = 'Файлы измерений не найдены'
const METRICS_NOT_FOUND_ERROR_MESSAGE = 'Измерения не найдены'

class Resolver {
  constructor () {
    this.allPaths = []
    this.paths = []
    this.years = []
    this.files = []
    this.metrics = []
    this.latitude = undefined
    this.longitude = undefined
  }

  // mutation
  async setRoot ({ root }) {
    this.allPaths = await getFilePaths(root)
    this.paths = selectValidPaths(this.allPaths)
    if (!this.paths.length) {
      throw new Error(FILES_NOT_FOUND_ERROR_MESSAGE)
    }
    return {
      filesCount: this.paths.length,
      invalidPaths: selectInvalidPaths(this.allPaths),
      years: getYearsFromPaths(this.paths)
    }
  }

  // mutation
  async loadDatabase ({ years }) {
    this.years = years
    this.paths = selectValidPaths(this.allPaths)
    this.paths = filterPaths(this.paths, { years })
    this.allFiles = await readFiles(this.paths, BULK_SIZE)
    this.files = selectValidFiles(this.allFiles)
    this.metrics = createMetricsDatabase(this.files)
    if (!this.metrics.length) {
      throw new Error(METRICS_NOT_FOUND_ERROR_MESSAGE)
    }
    sortMetrics(this.metrics)
    return {
      filesCount: this.paths.length,
      metricsCount: this.metrics.length,
      missedMetricsCount: _getMissedMetricsCountForDatabase(
        this.metrics, this.years
      ),
      brokenMetricsCount: getBrokenMetricsCount(this.metrics),
      minDate: this.metrics[0].date.unix(),
      maxDate: this.metrics[this.metrics.length - 1].date.unix()
    }
  }

  // mutation
  setLatLong ({ latitude, longitude }) {
    this.latitude = latitude
    this.longitude = longitude
    return true
  }

  // mutation
  async exportCalendarYear ({ year, path, metricsName, open }) {
    const calendarYear = getCalendarYear(
      this.metrics, year, this.latitude, this.longitude
    )
    await exportCalendarYear(calendarYear, year, path, metricsName)
    if (open) {
      await opn(path)
    }
    return path
  }

  // mutation
  async exportCalendarMonth ({ year, month, path, metricsName, open }) {
    const calendarMonth = getCalendarMonth(
      this.metrics, year, month, this.latitude, this.longitude
    )
    await exportCalendarMonth(calendarMonth, year, month, path, metricsName)
    if (open) {
      await opn(path)
    }
    return path
  }

  // mutation
  exportCalendar ({ type, year, month, path, metricsName, open }) {
    if (type === 'YEAR') {
      return this.exportCalendarYear({ year, path, metricsName, open })
    }
    return this.exportCalendarMonth({ year, month, path, metricsName, open })
  }

  // mutation
  async exportTable ({ start, end, path, grouping, addHours, metricsName, open }) {
    if (!grouping) {
      grouping = 1
    }
    if (!addHours) {
      addHours = 0
    }
    const table = getTable(this.metrics, start, end, grouping, addHours)
    await exportTable(table, path, metricsName)
    if (open) {
      await opn(path)
    }
    return path
  }

  // query
  calendarYear ({ year }) {
    return getCalendarYear(this.metrics, year, this.latitude, this.longitude)
  }

  // query
  calendarMonth ({ year, month }) {
    return getCalendarMonth(
      this.metrics, year, month, this.latitude, this.longitude
    )
  }

  // query
  calendar ({ type, year, month }) {
    if (type === 'YEAR') {
      const calendarMonths = this.calendarYear({ year })
      return calendarMonths.map((m) => ({
        ...m,
        __typename: 'CalendarMonth'
      }))
    }
    const calendarDays = this.calendarMonth({ year, month })
    return calendarDays.map((d) => ({
      ...d,
      __typename: 'CalendarDay'
    }))
  }

  // query
  chartYear ({ year, metricsName }) {
    return getChartYear(
      this.metrics, metricsName, year, this.latitude, this.longitude
    )
  }

  // query
  chartMonth ({ year, month, metricsName }) {
    return getChartMonth(
      this.metrics, metricsName, year, month, this.latitude, this.longitude
    )
  }

  // query
  chartTable ({ start, end, metricsName, grouping, addHours }) {
    return getChartTable(
      this.metrics, metricsName, start, end, grouping, addHours
    )
  }

  // guery
  table ({ start, end, grouping, addHours }) {
    if (!grouping) {
      grouping = 1
    }
    if (!addHours) {
      addHours = 0
    }
    return getTable(this.metrics, start, end, grouping, addHours)
  }
}

module.exports = new Resolver()

/// ----------------------------------------------------------------------------

/**
 * @private
 * @param {Array.<Metrics>} metrics
 * @param {Array.<number>} years
 */
function _getMissedMetricsCountForDatabase (metrics, years) {
  let days = 0
  for (let year of years) {
    for (let i = 0; i < 12; i++) {
      days += moment(`${year}-${i + 1}`, 'YYYY-MM').daysInMonth()
    }
  }
  return getMissedMetricsCount(metrics, days)
}
