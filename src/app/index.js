const pt = require('path')
const fs = require('fs')
const express = require('express')
const graphqlHTTP = require('express-graphql')
const { buildSchema } = require('graphql')
const cors = require('cors')
const rootValue = require('./resolver')

/**
 * @typedef {import('express').Application} Application
 */

/**
 * @param {{graphiql?:boolean, corsOrigin?:string}} opts
 * @returns {Application}
 */
function createApp (opts = {}) {
  const app = express()
  const { corsOrigin: origin } = opts
  if (origin) {
    app.use(cors({ origin }))
  }
  const schema = buildSchema(fs.readFileSync(
    pt.join(__dirname, 'schema.graphql'),
    'utf8'
  ))
  const { graphiql } = opts
  app.use('/graphql', graphqlHTTP({
    schema,
    rootValue,
    graphiql
  }))
  return app
}

module.exports = createApp

/// ----------------------------------------------------------------------------

/* eslint-disable no-console */
/* istanbul ignore next */
if (require.main === module) {
  const app = createApp({ graphiql: true })
  const port = 3000
  app.listen(port)
  console.log(
    `Running a GraphQL API server at http://localhost:${port}/graphql`
  )
}
