const {
  parseMetricsData,
  createEmptyMetricsData
} = require('./db/metrics')

module.exports = {
  parseMetricsData,
  createEmptyMetricsData
}
