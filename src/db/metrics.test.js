const moment = require('moment')
const {
  createMetricsDatabase,
  parseMetricsData,
  sortMetrics,
  getBrokenMetricsCount,
  getMissedMetricsCount,
  createEmptyMetricsData,
  aggregateMetrics
} = require('./metrics')

const meteoFiles = [{
  year: 2007,
  month: 5,
  day: 24,
  data: `
00:00:00  1,7  0,0  3,3 156  64 263   5,1   5,0   5,1 97 97 97   5,0   5,0   5,0  987,0  987,0  987,0  23,1  23,1  23,1 43 43 43   0   4,2   1,0   5,0
00:10:00  2,1  0,0  3,3 152  52 204   5,1   5,0   5,1 97 97 97   5,0   5,0   5,0  987,0  987,0  987,0  23,1  23,0  23,1 43 43 43   0   3,5   1,0   5,0
00:20:00  1,9  0,0  3,5 142  11 190   5,1   5,0   5,1 97 97 97   5,0   5,0   5,0  986,1  986,0  987,0  23,0  23,0  23,0 43 43 43   0   4,1   1,0   5,0
`.split('\n').join('\r\n')
}, {
  year: 2011,
  month: 2,
  day: 22,
  data: `
07:20:00  6,4  4,0  9,0 163  99 187  -0,7  -0,8  -0,7 86 86 86  -2,8  -2,8  -2,7  991,0  991,0  991,0  21,1  21,1  21,1 28 28 28   0 -12,1 -16,0  -8,0
07:50:00  6,6  2,7  9,4 168 123 198  -0,8  -0,8  -0,7 86 86 86  -2,8  -2,8  -2,7  991,0  991,0  991,0  21,1  21,1  21,1 28 28 28   0 -12,4 -16,0  -4,0
`.split('\n').join('\r\n')
}, {
  year: 2008,
  month: 6,
  day: 12,
  data: `
00:50:00  2,0  0,0  2,9 329   2 357  12,4  12,4  12,4 98 98 98  12,0  12,0  12,0 1004,0 1004,0 1004,0  21,7  21,7  21,7 53 53 53   0  11,6  10,0  12,0
01:00:00  2,0  1,4  3,3 332 291 357  12,4  12,3  12,4 98 98 98  12,0  12,0  12,0 1004,0 1004,0 1004,0  21,7  21,7  21,7 53 53 53   0  11,2   9,0  12,0
01:00:00  2,0  1,4  3,3 332 291 357  12,4  12,3  12,4 98 98 98  12,0  12,0  12,0 1004,0 1004,0 1004,0  21,7  21,7  21,7 53 53 53   0  11,2   9,0  12,0
01:10:00  1,6  0,0  2,5 332   2 357  12,3  12,3  12,4                            1004,0 1004,0 1004,0  21,7  21,7  21,7 53 53 53   0  11,9  10,0  12,0
01:20:00  1,4  0,0  2,3 344   0 357  12,4  12,4  12,5 98 98 98  12,1  12,0  13,0 1004,0 1004,0 1004,0  21,7  21,7  21,7 53 53 53   0  12,1  12,0  13,0
`.split('\n').join('\r\n')
}]

describe('createMetricsDatabase', () => {
  it('converts a list of meteo files to a list of metrics', () => {
    const database = createMetricsDatabase(meteoFiles)
    expect(database).toHaveLength(9)
    expect(database[0].date).toEqual(moment({
      year: 2007, month: 5, day: 24, hours: 0, minutes: 0, seconds: 0
    }))
    expect(database[0].data).toEqual([
      1.7, 0, 3.3, 156, 64, 263, 5.1, 5, 5.1, 97, 97, 97, 5, 5, 5, 987, 987,
      987, 23.1, 23.1, 23.1, 43, 43, 43, 0, 4.2, 1, 5.0
    ])
    expect(database[7].date).toEqual(moment({
      year: 2008, month: 6, day: 12, hours: 1, minutes: 10, seconds: 0
    }))
    expect(database[7].data).toEqual([
      1.6, 0, 2.5, 332, 2, 357, 12.3, 12.3, 12.4, null, null, null, null, null,
      null, 1004, 1004, 1004, 21.7, 21.7, 21.7, 53, 53, 53, 0, 11.9, 10, 12
    ])
  })
})

describe('parseMetricsData', () => {
  it('parses a metrics data array to the object', () => {
    const database = createMetricsDatabase(meteoFiles)
    const metricsShape = parseMetricsData(database[3].data)
    expect(metricsShape).toEqual({
      windSpeed: { mean: 6.4, min: 4, max: 9 },
      windDirection: { mean: 163, min: 99, max: 187 },
      outsideTemperature: { mean: -0.7, min: -0.8, max: -0.7 },
      outsideHumidity: { mean: 86, min: 86, max: 86 },
      outsideDewpoint: { mean: -2.8, min: -2.8, max: -2.7 },
      pressure: { mean: 991, min: 991, max: 991 },
      insideTemperature: { mean: 21.1, min: 21.1, max: 21.1 },
      insideHumidity: { mean: 28, min: 28, max: 28 },
      rain: 0,
      windChill: { mean: -12.1, min: -16, max: -8 }
    })
  })
})

describe('sortMetrics', () => {
  it('sorts metrics by date', () => {
    const database = createMetricsDatabase(meteoFiles)
    sortMetrics(database)
    expect(database[0].date.format('DD.MM.YY HH:mm')).toBe('24.06.07 00:00')
    expect(database[1].date.format('DD.MM.YY HH:mm')).toBe('24.06.07 00:10')
    expect(database[2].date.format('DD.MM.YY HH:mm')).toBe('24.06.07 00:20')
    expect(database[3].date.format('DD.MM.YY HH:mm')).toBe('12.07.08 00:50')
    expect(database[4].date.format('DD.MM.YY HH:mm')).toBe('12.07.08 01:00')
    expect(database[5].date.format('DD.MM.YY HH:mm')).toBe('12.07.08 01:10')
    expect(database[6].date.format('DD.MM.YY HH:mm')).toBe('12.07.08 01:20')
    expect(database[7].date.format('DD.MM.YY HH:mm')).toBe('22.03.11 07:20')
    expect(database[8].date.format('DD.MM.YY HH:mm')).toBe('22.03.11 07:50')
  })
})

describe('getBrokenMetricsCount', () => {
  it('counts the number of broken metrics', () => {
    const database = createMetricsDatabase(meteoFiles)
    const broken = getBrokenMetricsCount(database)
    expect(broken).toBe(1)
  })
})

describe('getMissedMetricsCount', () => {
  it('counts the number of missed metrics', () => {
    const database = createMetricsDatabase(meteoFiles)
    const missed = getMissedMetricsCount(database, 3)
    expect(missed).toBe(423)
  })
})

describe('createEmptyMetricsData', () => {
  it('creates an empty metrics data array', () => {
    expect(createEmptyMetricsData()).toHaveLength(28)
  })
})

describe('aggregateMetrics', () => {
  it('calculates aggregate metrics', () => {
    const database = createMetricsDatabase(meteoFiles)
    const aggregated = aggregateMetrics(database)
    const format = (n) => n ? n.toFixed(2) : n
    expect(aggregated.map(format)).toEqual([
      '2.86', '2.70', '4.39', '235.33', '80.50', '274.44', '7.03', '6.98',
      '7.07', '94.63', '94.63', '94.63', '5.69', '5.67', '5.83', '995.34',
      '995.33', '995.44', '22.02', '22.01', '22.02', '44.11', '44.11', '44.11',
      null, '3.79', '1.33', '5.78'
    ])
  })
})
