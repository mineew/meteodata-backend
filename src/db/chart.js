const moment = require('moment')
const { parseMetricsData } = require('./metrics')
const {
  getCalendarYear,
  getCalendarMonth
} = require('./calendar')
const {
  getTable
} = require('./table')

moment.locale('ru')

/**
 * @typedef {import('./metrics').Metrics} Metrics
 */

/**
 * @typedef {import('./calendar').CalendarDay} CalendarDay
 */

/**
 * @typedef {import('./calendar').CalendarMonth} CalendarMonth
 */

/**
 * @typedef {import('./table').TableRow} TableRow
 */

/**
 * @typedef {Object} ChartDataItem
 * @property {string} name
 * @property {number|null} mean
 * @property {number|null} min
 * @property {number|null} max
 */

/**
 * @typedef {Object} CalendarChart
 * @property {Array.<ChartDataItem>} fullTime
 * @property {Array.<ChartDataItem>} lightTime
 * @property {Array.<ChartDataItem>} darkTime
 */

/**
 * @param {Array.<Metrics>} metrics
 * @param {string} metricsName
 * @param {number} year
 * @param {number} latitude
 * @param {number} longitude
 * @returns {CalendarChart}
 */
function getChartYear (metrics, metricsName, year, latitude, longitude) {
  const calendarYear = getCalendarYear(metrics, year, latitude, longitude)
  const chartYear = _createCalendarChart(
    calendarYear,
    metricsName,
    ({ month }) => moment({ month }).format('MMM')
  )
  return _fillYearGaps(chartYear)
}

/**
 * @param {Array.<Metrics>} metrics
 * @param {string} metricsName
 * @param {number} year
 * @param {number} month
 * @param {number} latitude
 * @param {number} longitude
 * @returns {CalendarChart}
 */
function getChartMonth (
  metrics, metricsName, year, month, latitude, longitude
) {
  const calendarMonth = getCalendarMonth(
    metrics, year, month, latitude, longitude
  )
  const chartMonth = _createCalendarChart(
    calendarMonth,
    metricsName,
    ({ day }) => moment({ day }).format('DD')
  )
  return _fillMonthGaps(chartMonth, year, month)
}

/**
 * @param {Array.<Metrics>} metrics
 * @param {string} metricsName
 * @param {number} start
 * @param {number} end
 * @param {number} [grouping]
 * @param {number} [addHours]
 * @returns {Array.<ChartDataItem>}
 */
function getChartTable (
  metrics, metricsName, start, end, grouping = 1, addHours = 0
) {
  const table = getTable(metrics, start, end, grouping, addHours)
  const chartTable = []
  const isOneDay = _isOneDay(table)
  const isOneMonth = _isOneMonth(table)
  const isOneYear = _isOneYear(table)
  for (let row of table) {
    chartTable.push(_createChartDataItem(row.data, metricsName, _getRowName(
      row, isOneDay, isOneMonth, isOneYear
    )))
  }
  return chartTable
}

module.exports = {
  getChartYear,
  getChartMonth,
  getChartTable
}

/// ----------------------------------------------------------------------------

/**
 * @param {Array.<number|null>} metricsData
 * @param {string} metricsName
 * @param {string} name
 * @returns {ChartDataItem}
 */
function _createChartDataItem (metricsData, metricsName, name) {
  const parsed = parseMetricsData(metricsData)
  const metrics = parsed.hasOwnProperty(metricsName)
    ? parsed[metricsName]
    : null
  if (typeof metrics === 'number' || metrics === null) {
    return { name, mean: metrics, min: metrics, max: metrics }
  }
  return { name, ...metrics }
}

/**
 * @private
 * @param {Array.<CalendarDay|CalendarMonth>} calendarData
 * @param {string} metricsName
 * @param {function(CalendarDay|CalendarMonth):string} getName
 * @returns {CalendarChart}
 */
function _createCalendarChart (calendarData, metricsName, getName) {
  const calendarChart = { fullTime: [], lightTime: [], darkTime: [] }
  for (let calendarDataItem of calendarData) {
    calendarChart.fullTime.push(_createChartDataItem(
      calendarDataItem.fullTime,
      metricsName,
      getName(calendarDataItem)
    ))
    calendarChart.lightTime.push(_createChartDataItem(
      calendarDataItem.lightTime,
      metricsName,
      getName(calendarDataItem)
    ))
    calendarChart.darkTime.push(_createChartDataItem(
      calendarDataItem.darkTime,
      metricsName,
      getName(calendarDataItem)
    ))
  }
  return calendarChart
}

/**
 * @param {CalendarChart} calendarChart
 * @returns {CalendarChart}
 */
function _fillYearGaps (calendarChart) {
  const filled = { fullTime: [], lightTime: [], darkTime: [] }
  const nulls = { mean: null, min: null, max: null }
  for (let i = 0; i < 12; i++) {
    const monthName = moment({ month: i }).format('MMM')
    for (let key of ['fullTime', 'lightTime', 'darkTime']) {
      const found = calendarChart[key].find(
        ({ name }) => name === monthName
      )
      filled[key].push(found || { name: monthName, ...nulls })
    }
  }
  return filled
}

/**
 * @param {CalendarChart} calendarChart
 * @param {number} year
 * @param {number} month
 * @returns {CalendarChart}
 */
function _fillMonthGaps (calendarChart, year, month) {
  const filled = { fullTime: [], lightTime: [], darkTime: [] }
  const nulls = { mean: null, min: null, max: null }
  const daysInMonth = moment({ year, month }).daysInMonth()
  for (let i = 1; i <= daysInMonth; i++) {
    const dayName = moment({ day: i }).format('DD')
    for (let key of ['fullTime', 'lightTime', 'darkTime']) {
      const found = calendarChart[key].find(
        ({ name }) => name === dayName
      )
      filled[key].push(found || { name: dayName, ...nulls })
    }
  }
  return filled
}

/**
 * @private
 * @param {Array.<TableRow>} table
 * @returns {boolean}
 */
function _isOneDay (table) {
  return table.every(({ date }) => date === table[0].date)
}

/**
 * @private
 * @param {Array.<TableRow>} table
 * @returns {boolean}
 */
function _isOneMonth (table) {
  const month = (date) => date.split('.')[1]
  return table.every(({ date }) => month(date) === month(table[0].date))
}

/**
 * @private
 * @param {Array.<TableRow>} table
 * @returns {boolean}
 */
function _isOneYear (table) {
  const year = (date) => date.split('.')[2]
  return table.every(({ date }) => year(date) === year(table[0].date))
}

/**
 * @private
 * @param {TableRow} row
 * @param {boolean} isOneDay
 * @param {boolean} isOneMonth
 * @param {boolean} isOneYear
 * @returns {string}
 */
function _getRowName (row, isOneDay, isOneMonth, isOneYear) {
  const [ day, month ] = row.date.split('.')
  if (isOneDay) {
    return row.time
  }
  if (isOneMonth) {
    return `${day} ${row.time}`
  }
  if (isOneYear) {
    return `${day}.${month} ${row.time}`
  }
  return `${row.date} ${row.time}`
}
