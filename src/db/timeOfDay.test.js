const moment = require('moment')
const {
  selectLightTimeMetrics,
  selectDarkTimeMetrics
} = require('./timeOfDay')

const metrics = []

for (let hour = 0; hour < 24; hour++) {
  const hourf = hour < 10 ? `0${hour}` : hour.toString()
  metrics.push({
    date: moment(`10.07.2018 ${hourf}:00`, 'DD.MM.YYYY HH:mm'),
    data: null
  })
}

// Архангельск
const arhLat = 64.5
const arhLong = 40.5

// Москва
const mosLat = 55.75
const mosLong = 37.5

describe('selectLightTimeMetrics', () => {
  it('selects the light time metrics', () => {
    const arh = selectLightTimeMetrics(metrics, arhLat, arhLong)
    expect(arh.map((m) => m.date.format('HH:mm'))).toEqual([
      '03:00',
      '04:00',
      '05:00',
      '06:00',
      '07:00',
      '08:00',
      '09:00',
      '10:00',
      '11:00',
      '12:00',
      '13:00',
      '14:00',
      '15:00',
      '16:00',
      '17:00',
      '18:00',
      '19:00',
      '20:00',
      '21:00',
      '22:00'
    ])
    const mos = selectLightTimeMetrics(metrics, mosLat, mosLong)
    expect(mos.map((m) => m.date.format('HH:mm'))).toEqual([
      '04:00',
      '05:00',
      '06:00',
      '07:00',
      '08:00',
      '09:00',
      '10:00',
      '11:00',
      '12:00',
      '13:00',
      '14:00',
      '15:00',
      '16:00',
      '17:00',
      '18:00',
      '19:00',
      '20:00',
      '21:00'
    ])
  })
})

describe('selectDarkTimeMetrics', () => {
  it('selects the dark time metrics', () => {
    const arh = selectDarkTimeMetrics(metrics, arhLat, arhLong)
    expect(arh.map((m) => m.date.format('HH:mm'))).toEqual([
      '00:00',
      '01:00',
      '02:00',
      '23:00'
    ])
    const mos = selectDarkTimeMetrics(metrics, mosLat, mosLong)
    expect(mos.map((m) => m.date.format('HH:mm'))).toEqual([
      '00:00',
      '01:00',
      '02:00',
      '03:00',
      '22:00',
      '23:00'
    ])
  })
})
