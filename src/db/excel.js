const moment = require('moment')
const { Workbook } = require('exceljs')
const { parseMetricsData } = require('./metrics')

require('moment/locale/ru')
moment.locale('ru')

/**
 * @typedef {import('exceljs').Worksheet} Worksheet
 */

/**
 * @typedef {import('exceljs').Cell} Cell
 */

/**
 * @typedef {import('./calendar').CalendarMonth} CalendarMonth
 */

/**
 * @typedef {import('./calendar').CalendarDay} CalendarDay
 */

/**
 * @typedef {import('./table').TableRow} TableRow
 */

/**
 * @param {Array.<CalendarMonth>} calendarYear
 * @param {number} year
 * @param {string} filename
 * @param {string} [metricsName]
 * @returns {Promise.<void>}
 */
function exportCalendarYear (calendarYear, year, filename, metricsName = '') {
  const workbook = new Workbook()
  const sheets = {
    fullTime: workbook.addWorksheet('Сутки'),
    lightTime: workbook.addWorksheet('Светлое время суток'),
    darkTime: workbook.addWorksheet('Темное время суток')
  }
  for (let sheet of Object.values(sheets)) {
    _writeCalendarYearHead(sheet, metricsName)
  }
  let rowNumber = 3
  for (let calendarMonth of calendarYear) {
    for (let type of Object.keys(sheets)) {
      const sheet = sheets[type]
      _writeCalendarMonth(sheet, rowNumber, calendarMonth, year, type, metricsName)
    }
    rowNumber += 1
  }
  return workbook.xlsx.writeFile(filename)
}

/**
 * @param {Array.<CalendarDay>} calendarMonth
 * @param {number} year
 * @param {number} month
 * @param {string} filename
 * @param {string} [metricsName]
 * @returns {Promise.<void>}
 */
function exportCalendarMonth (calendarMonth, year, month, filename, metricsName = '') {
  const workbook = new Workbook()
  const sheets = {
    fullTime: workbook.addWorksheet('Сутки'),
    lightTime: workbook.addWorksheet('Светлое время суток'),
    darkTime: workbook.addWorksheet('Темное время суток')
  }
  for (let sheet of Object.values(sheets)) {
    _writeCalendarMonthHead(sheet, metricsName)
  }
  let rowNumber = 3
  for (let calendarDay of calendarMonth) {
    for (let type of Object.keys(sheets)) {
      const sheet = sheets[type]
      _writeCalendarDay(
        sheet, rowNumber, calendarDay, year, month, type, metricsName
      )
    }
    rowNumber += 1
  }
  return workbook.xlsx.writeFile(filename)
}

/**
 * @param {Array.<TableRow>} table
 * @param {string} filename
 * @param {string} [metricsName]
 * @returns {Promise.<void>}
 */
function exportTable (table, filename, metricsName = '') {
  const workbook = new Workbook()
  const sheet = workbook.addWorksheet()
  _writeTableHead(sheet, metricsName)
  let rowNumber = 3
  for (let tableRow of table) {
    _writeTableRow(sheet, rowNumber, tableRow, metricsName)
    rowNumber += 1
  }
  return workbook.xlsx.writeFile(filename)
}

module.exports = {
  exportCalendarYear,
  exportCalendarMonth,
  exportTable
}

/// ----------------------------------------------------------------------------

const metricsNameMap = {
  windSpeed: 'Скорость ветра, м/сек',
  windDirection: 'Направление ветра, °',
  outsideTemperature: 'Температура улицы, °С',
  outsideHumidity: 'Влажность улицы, %',
  outsideDewpoint: 'Точка росы, °С',
  pressure: 'Давление, hPa',
  insideTemperature: 'Температура помещения, °С',
  insideHumidity: 'Влажность помещения, %',
  rain: 'Осадки, мм',
  windChill: 'Температура порыва ветра, °С'
}

/**
 * @private
 * @param {Worksheet} sheet
 * @param {number} startColumn
 * @param {string} [metricsName]
 */
function _writeHead (sheet, startColumn, metricsName) {
  if (metricsName) {
    sheet.getCell(1, startColumn).value = metricsNameMap[metricsName]
    if (metricsName === 'rain') {
      _alignCenter(sheet.getCell(1, startColumn))
      _bordered(sheet.getCell(1, startColumn))
      sheet.getCell(2, startColumn).value = 'сумма'
      _alignCenter(sheet.getCell(2, startColumn))
      _bordered(sheet.getCell(2, startColumn))
    } else {
      _alignCenter(sheet.getCell(1, startColumn))
      _bordered(sheet.getCell(1, startColumn))
      sheet.mergeCells(1, startColumn, 1, startColumn + 2)
      sheet.getCell(2, startColumn).value = 'средн'
      _alignCenter(sheet.getCell(2, startColumn))
      _bordered(sheet.getCell(2, startColumn))
      sheet.getCell(2, startColumn + 1).value = 'мин'
      _alignCenter(sheet.getCell(2, startColumn + 1))
      _bordered(sheet.getCell(2, startColumn + 1))
      sheet.getCell(2, startColumn + 2).value = 'макс'
      _alignCenter(sheet.getCell(2, startColumn + 2))
      _bordered(sheet.getCell(2, startColumn + 2))
    }
    return
  }
  sheet.getCell(1, startColumn).value = metricsNameMap.windSpeed
  sheet.getCell(1, startColumn + 3).value = metricsNameMap.windDirection
  sheet.getCell(1, startColumn + 6).value = metricsNameMap.outsideTemperature
  sheet.getCell(1, startColumn + 9).value = metricsNameMap.outsideHumidity
  sheet.getCell(1, startColumn + 12).value = metricsNameMap.outsideDewpoint
  sheet.getCell(1, startColumn + 15).value = metricsNameMap.pressure
  sheet.getCell(1, startColumn + 18).value = metricsNameMap.insideTemperature
  sheet.getCell(1, startColumn + 21).value = metricsNameMap.insideHumidity
  sheet.getCell(1, startColumn + 24).value = metricsNameMap.rain
  sheet.getCell(1, startColumn + 25).value = metricsNameMap.windChill
  for (let i = startColumn; i <= startColumn + 23; i += 3) {
    _alignCenter(sheet.getCell(1, i))
    _bordered(sheet.getCell(1, i))
    sheet.mergeCells(1, i, 1, i + 2)
    sheet.getCell(2, i).value = 'средн'
    _alignCenter(sheet.getCell(2, i))
    _bordered(sheet.getCell(2, i))
    sheet.getCell(2, i + 1).value = 'мин'
    _alignCenter(sheet.getCell(2, i + 1))
    _bordered(sheet.getCell(2, i + 1))
    sheet.getCell(2, i + 2).value = 'макс'
    _alignCenter(sheet.getCell(2, i + 2))
    _bordered(sheet.getCell(2, i + 2))
  }
  _alignCenter(sheet.getCell(1, startColumn + 24))
  _bordered(sheet.getCell(1, startColumn + 24))
  _bordered(sheet.getCell(1, startColumn + 25))
  sheet.mergeCells(1, startColumn + 25, 1, startColumn + 27)
  sheet.getCell(2, startColumn + 24).value = 'сумма'
  _alignCenter(sheet.getCell(2, startColumn + 24))
  _bordered(sheet.getCell(2, startColumn + 24))
  sheet.getCell(2, startColumn + 25).value = 'средн'
  _alignCenter(sheet.getCell(2, startColumn + 25))
  _bordered(sheet.getCell(2, startColumn + 25))
  sheet.getCell(2, startColumn + 26).value = 'мин'
  _alignCenter(sheet.getCell(2, startColumn + 26))
  _bordered(sheet.getCell(2, startColumn + 26))
  sheet.getCell(2, startColumn + 27).value = 'макс'
  _alignCenter(sheet.getCell(2, startColumn + 27))
  _bordered(sheet.getCell(2, startColumn + 27))
}

/**
 * @private
 * @param {Worksheet} sheet
 * @param {string} firstColumnLabel
 * @param {string} [metricsName]
 */
function _writeCalendarHead (sheet, firstColumnLabel, metricsName) {
  sheet.getCell(1, 1).value = firstColumnLabel
  _alignCenter(sheet.getCell(1, 1))
  _bordered(sheet.getCell(1, 1))
  sheet.mergeCells(1, 1, 2, 1)
  _writeHead(sheet, 2, metricsName)
}

/**
 * @private
 * @param {Worksheet} sheet
 * @param {string} [metricsName]
 */
function _writeCalendarYearHead (sheet, metricsName) {
  _writeCalendarHead(sheet, 'Месяц', metricsName)
}

/**
 * @private
 * @param {Worksheet} sheet
 * @param {string} [metricsName]
 */
function _writeCalendarMonthHead (sheet, metricsName) {
  _writeCalendarHead(sheet, 'Дата', metricsName)
}

/**
 * @private
 * @param {Worksheet} sheet
 * @param {string} [metricsName]
 */
function _writeTableHead (sheet, metricsName) {
  sheet.getCell(1, 1).value = 'Дата'
  _alignCenter(sheet.getCell(1, 1))
  _bordered(sheet.getCell(1, 1))
  sheet.mergeCells(1, 1, 2, 1)
  sheet.getCell(1, 2).value = 'Время'
  _alignCenter(sheet.getCell(1, 2))
  _bordered(sheet.getCell(1, 2))
  sheet.mergeCells(1, 2, 2, 2)
  _writeHead(sheet, 3, metricsName)
}

/**
 * @private
 * @param {Worksheet} sheet
 * @param {number} startColumn
 * @param {number} rowNumber
 * @param {Array.<number|null>} data
 */
function _writeRow (sheet, startColumn, rowNumber, data) {
  for (let i = 0; i < data.length; i++) {
    if (data[i] !== null) {
      sheet.getCell(rowNumber, i + startColumn).value = data[i]
      sheet.getCell(rowNumber, i + startColumn).numFmt = '0.000'
    } else {
      sheet.getCell(rowNumber, i + startColumn).value = ''
    }
    _alignCenter(sheet.getCell(rowNumber, i + startColumn))
    _bordered(sheet.getCell(rowNumber, i + startColumn))
  }
}

/**
 * @private
 * @param {Worksheet} sheet
 * @param {number} rowNumber
 * @param {CalendarMonth} calendarMonth
 * @param {number} year
 * @param {'fullTime'|'lightTime'|'darkTime'} type
 * @param {string} [metricsName]
 */
function _writeCalendarMonth (sheet, rowNumber, calendarMonth, year, type, metricsName) {
  let data = calendarMonth[type]
  if (metricsName) {
    const parsed = parseMetricsData(calendarMonth[type])
    if (metricsName === 'rain') {
      data = [parsed[metricsName]]
    } else {
      data = [
        parsed[metricsName].mean,
        parsed[metricsName].min,
        parsed[metricsName].max
      ]
    }
  }
  sheet.getCell(rowNumber, 1).value = _getMonthName(year, calendarMonth.month)
  _alignCenter(sheet.getCell(rowNumber, 1))
  _bordered(sheet.getCell(rowNumber, 1))
  _writeRow(sheet, 2, rowNumber, data)
}

/**
 * @private
 * @param {Worksheet} sheet
 * @param {number} rowNumber
 * @param {CalendarDay} calendarDay
 * @param {number} year
 * @param {number} month
 * @param {'fullTime'|'lightTime'|'darkTime'} type
 * @param {string} [metricsName]
 */
function _writeCalendarDay (sheet, rowNumber, calendarDay, year, month, type, metricsName) {
  let data = calendarDay[type]
  if (metricsName) {
    const parsed = parseMetricsData(calendarDay[type])
    if (metricsName === 'rain') {
      data = [parsed[metricsName]]
    } else {
      data = [
        parsed[metricsName].mean,
        parsed[metricsName].min,
        parsed[metricsName].max
      ]
    }
  }
  sheet.getCell(rowNumber, 1).value = _getDayName(year, month, calendarDay.day)
  _alignCenter(sheet.getCell(rowNumber, 1))
  _bordered(sheet.getCell(rowNumber, 1))
  _writeRow(sheet, 2, rowNumber, data)
}

/**
 * @private
 * @param {Worksheet} sheet
 * @param {number} rowNumber
 * @param {TableRow} tableRow
 * @param {string} [metricsName]
 */
function _writeTableRow (sheet, rowNumber, tableRow, metricsName) {
  let data = tableRow.data
  if (metricsName) {
    const parsed = parseMetricsData(tableRow.data)
    if (metricsName === 'rain') {
      data = [parsed[metricsName]]
    } else {
      data = [
        parsed[metricsName].mean,
        parsed[metricsName].min,
        parsed[metricsName].max
      ]
    }
  }
  sheet.getCell(rowNumber, 1).value = tableRow.date
  _alignCenter(sheet.getCell(rowNumber, 1))
  _bordered(sheet.getCell(rowNumber, 1))
  sheet.getCell(rowNumber, 2).value = tableRow.time
  _alignCenter(sheet.getCell(rowNumber, 2))
  _bordered(sheet.getCell(rowNumber, 2))
  _writeRow(sheet, 3, rowNumber, data)
}

/**
 * @private
 * @param {number} year
 * @param {number} month
 * @returns {string}
 */
function _getMonthName (year, month) {
  return moment(`${year}-${month + 1}`, 'YYYY-MM').format('MMMM')
}

/**
 * @private
 * @param {number} year
 * @param {number} month
 * @param {number} day
 * @returns {string}
 */
function _getDayName (year, month, day) {
  return moment({ year, month, day }).format('DD.MM.YYYY')
}

/**
 * @private
 * @param {Cell} cell
 */
function _alignCenter (cell) {
  cell.alignment = { horizontal: 'center', vertical: 'middle' }
}

/**
 * @private
 * @param {Cell} cell
 */
function _bordered (cell) {
  cell.border = {
    top: { style: 'thin' },
    bottom: { style: 'thin' },
    left: { style: 'thin' },
    right: { style: 'thin' }
  }
}
