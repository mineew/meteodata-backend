const fs = require('fs')
const { promisify } = require('util')
const pt = require('path')
const unzipper = require('unzipper')
const rimraf = require('rimraf')
const { Workbook } = require('exceljs')
const moment = require('moment')
const {
  createEmptyMetricsData
} = require('./metrics')
const {
  getCalendarYear,
  getCalendarMonth
} = require('./calendar')
const {
  getTable
} = require('./table')
const {
  exportCalendarYear,
  exportCalendarMonth,
  exportTable
} = require('./excel')

const readFile = promisify(fs.readFile)

const metrics = []

for (let year of [2011, 2012]) {
  for (let month = 0; month < 12; month++) {
    for (let day = 1; day <= 31; day++) {
      for (let hour = 0; hour < 24; hour++) {
        for (let minute = 0; minute <= 50; minute += 10) {
          const monthf = month < 10 ? `0${month}` : month.toString()
          const dayf = day < 10 ? `0${day}` : day.toString()
          const hourf = hour < 10 ? `0${hour}` : hour.toString()
          const minutef = minute < 10 ? `0${minute}` : minute.toString()
          const date = moment(
            `${dayf}.${monthf}.${year} ${hourf}:${minutef}`, 'DD.MM.YYYY HH:mm'
          )
          const data = createEmptyMetricsData().fill(day + hour + minute)
          metrics.push({ date, data })
        }
      }
    }
  }
}

// Архангельск
const arhLat = 19.1
const arhLong = 73.05

beforeAll(() => {
  return new Promise((resolve, reject) => {
    const zip = fs.createReadStream(pt.join(__dirname, 'excel.test.zip'))
    const unzip = zip.pipe(unzipper.Extract({ path: __dirname }))
    unzip.on('close', () => { resolve() })
    unzip.on('error', (error) => { reject(error) })
  })
})

afterAll(() => {
  const globs = ['**/*.xlsx', '**/*.csv']
  return Promise.all(globs.map((glob) => new Promise((resolve, reject) => {
    rimraf(glob, (error) => {
      error ? reject(error) : resolve()
    })
  })))
})

/**
 * @param {string} xlsxPath
 * @param {string} csvPath
 * @returns {boolean}
 */
async function compareExcel (xlsxPath, csvPath) {
  const workbook = new Workbook()
  await workbook.xlsx.readFile(xlsxPath)
  const exportToCsvPath = xlsxPath.replace('.xlsx', '.csv')
  await workbook.csv.writeFile(exportToCsvPath)
  const [ csvA, csvB ] = await Promise.all([
    readFile(exportToCsvPath, 'utf8'),
    readFile(csvPath, 'utf8')
  ])
  return csvA.trim() === csvB.trim()
}

describe('exportCalendarYear', () => {
  it('exports a calendar year data to an excel file', async () => {
    const calendarYear = getCalendarYear(metrics, 2011, arhLat, arhLong)
    const xlsxFilename = pt.join(__dirname, 'year-test.xlsx')
    const csvFilename = pt.join(__dirname, 'year.csv')
    await exportCalendarYear(calendarYear, 2011, xlsxFilename)
    const equal = await compareExcel(xlsxFilename, csvFilename)
    expect(equal).toBeTruthy()
  })
})

describe('exportCalendarMonth', () => {
  it('exports a calendar month to an excel file', async () => {
    const calendarMonth = getCalendarMonth(metrics, 2012, 6, arhLat, arhLong)
    const xlsxFilename = pt.join(__dirname, 'month-test.xlsx')
    const csvFilename = pt.join(__dirname, 'month.csv')
    await exportCalendarMonth(calendarMonth, 2011, 6, xlsxFilename)
    const equal = await compareExcel(xlsxFilename, csvFilename)
    expect(equal).toBeTruthy()
  })
})

describe('exportTable', () => {
  it('exports a table to an excel file', async () => {
    const start = moment('02.11.2011', 'DD.MM.YYYY').unix()
    const end = moment('03.11.2011', 'DD.MM.YYYY').unix()
    const table = getTable(metrics, start, end)
    const xlsxFilename = pt.join(__dirname, 'table-test.xlsx')
    const csvFilename = pt.join(__dirname, 'table.csv')
    await exportTable(table, xlsxFilename)
    const equal = await compareExcel(xlsxFilename, csvFilename)
    expect(equal).toBeTruthy()
  })

  it('can export gaps', async () => {
    const metricsWithGaps = metrics.filter(
      (m) => m.date.format('DD.MM.YYYY HH:mm') !== '02.11.2011 10:10' &&
             m.date.format('DD.MM.YYYY HH:mm') !== '02.11.2011 11:10' &&
             m.date.format('DD.MM.YYYY HH:mm') !== '02.11.2011 12:20' &&
             m.date.format('DD.MM.YYYY HH:mm') !== '02.11.2011 12:50' &&
             m.date.format('DD.MM.YYYY HH:mm') !== '02.11.2011 14:40'
    )
    const start = moment('02.11.2011', 'DD.MM.YYYY').unix()
    const end = moment('03.11.2011', 'DD.MM.YYYY').unix()
    const table = getTable(metricsWithGaps, start, end)
    const xlsxFilename = pt.join(__dirname, 'table-with-gaps-test.xlsx')
    const csvFilename = pt.join(__dirname, 'table-with-gaps.csv')
    await exportTable(table, xlsxFilename)
    const equal = await compareExcel(xlsxFilename, csvFilename)
    expect(equal).toBeTruthy()
  })
})
