const moment = require('moment')
const {
  createEmptyMetricsData
} = require('./metrics')
const {
  getCalendarYear,
  getCalendarMonth
} = require('./calendar')

const metrics = []

for (let year of [2011, 2012]) {
  for (let month of [7, 8]) {
    for (let day = 1; day <= 31; day++) {
      for (let hour = 0; hour < 24; hour++) {
        const monthf = `0${month}`
        const dayf = day < 10 ? `0${day}` : day.toString()
        const hourf = hour < 10 ? `0${hour}` : hour.toString()
        const date = moment(
          `${dayf}.${monthf}.${year} ${hourf}:00`, 'DD.MM.YYYY HH:mm'
        )
        const data = createEmptyMetricsData().fill(day)
        if (day === 15 && hour >= 10) {
          data[10] = null
        }
        metrics.push({ date, data })
      }
    }
  }
}

// Архангельск
const arhLat = 64.5
const arhLong = 40.5

const format = (n) => n ? n.toFixed(2) : n

describe('getCalendarYear', () => {
  it('creates a dataset suitable for calendar year', () => {
    const calYear = getCalendarYear(metrics, 2011, arhLat, arhLong)
    expect(calYear).toHaveLength(2)
    expect(calYear[0].month).toBe(6)
    expect(calYear[0].totalCount).toBe(31 * 24)
    expect(calYear[0].missedCount).toBe(31 * 144 - 31 * 24)
    expect(calYear[0].brokenCount).toBe(24 - 10)
    // fullTime
    const fullTime = createEmptyMetricsData().fill(16)
    fullTime[10] = 16.02
    fullTime[24] = 11904
    expect(calYear[0].fullTime.map(format)).toEqual(fullTime.map(format))
    // lightTime
    const lightTime = createEmptyMetricsData().fill(15.59)
    lightTime[10] = 15.61
    lightTime[24] = 9527
    expect(calYear[0].lightTime.map(format)).toEqual(lightTime.map(format))
    // darkTime
    const darkTime = createEmptyMetricsData().fill(17.87)
    darkTime[24] = 2377
    expect(calYear[0].darkTime.map(format)).toEqual(darkTime.map(format))
  })
})

describe('getCalendarMonth', () => {
  it('creates a dataset suitable for calendar month', () => {
    const calMonth = getCalendarMonth(metrics, 2012, 6, arhLat, arhLong)
    expect(calMonth).toHaveLength(31)
    expect(calMonth[14].day).toBe(15)
    expect(calMonth[14].totalCount).toBe(24)
    expect(calMonth[14].missedCount).toBe(144 - 24)
    expect(calMonth[14].brokenCount).toBe(24 - 10)
    // fullTime
    const fullTime = createEmptyMetricsData().fill(15)
    fullTime[24] = 360
    expect(calMonth[14].fullTime.map(format)).toEqual(fullTime.map(format))
    // lightTime
    const lightTime = createEmptyMetricsData().fill(15)
    lightTime[24] = 300
    expect(calMonth[14].lightTime.map(format)).toEqual(lightTime.map(format))
    // darkTime
    const darkTime = createEmptyMetricsData().fill(15)
    darkTime[24] = 60
    expect(calMonth[14].darkTime.map(format)).toEqual(darkTime.map(format))
  })

  it('collects data only about the days that are in the database', () => {
    const metricsWithoutSomeDays = metrics.filter(
      (m) => m.date.date() < 20
    )
    const calendarMonth = getCalendarMonth(metricsWithoutSomeDays, 2012, 6)
    expect(calendarMonth).toHaveLength(19)
  })
})
