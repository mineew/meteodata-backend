const moment = require('moment')

/**
 * @typedef {import('moment').Moment} Moment
 */

/**
 * @typedef {import('./fs').MeteoFile} MeteoFile
 */

/**
 * @typedef {Object} Metrics
 * @property {Moment} date
 * @property {Array.<number|null>} data
 */

/**
 * @param {Array.<MeteoFile>} files
 * @returns {Array.<Metrics>}
 */
function createMetricsDatabase (files) {
  const database = []
  const exists = {}
  for (let file of files) {
    const matrix = _convertFileDataToMatrix(file)
    for (let row of matrix) {
      const metrics = _convertMatrixRowToMetrics(file, row)
      const timestamp = metrics.date.unix()
      if (!exists[timestamp]) {
        database.push(metrics)
        exists[timestamp] = true
      }
    }
  }
  return database
}

/**
 * @typedef {Object} MeanMinMax
 * @property {number|null} mean
 * @property {number|null} min
 * @property {number|null} max
 */

/**
 * @typedef {Object} MetricsDataShape
 * @property {MeanMinMax} windSpeed
 * @property {MeanMinMax} windDirection
 * @property {MeanMinMax} outsideTemperature
 * @property {MeanMinMax} outsideHumidity
 * @property {MeanMinMax} outsideDewpoint
 * @property {MeanMinMax} pressure
 * @property {MeanMinMax} insideTemperature
 * @property {MeanMinMax} insideHumidity
 * @property {number|null} rain
 * @property {MeanMinMax} windChill
 */

/**
 * @param {Array.<number|null} data
 * @returns {MetricsDataShape}
 */
function parseMetricsData (data) {
  return {
    windSpeed: { mean: data[0], min: data[1], max: data[2] },
    windDirection: { mean: data[3], min: data[4], max: data[5] },
    outsideTemperature: { mean: data[6], min: data[7], max: data[8] },
    outsideHumidity: { mean: data[9], min: data[10], max: data[11] },
    outsideDewpoint: { mean: data[12], min: data[13], max: data[14] },
    pressure: { mean: data[15], min: data[16], max: data[17] },
    insideTemperature: { mean: data[18], min: data[19], max: data[20] },
    insideHumidity: { mean: data[21], min: data[22], max: data[23] },
    rain: data[24],
    windChill: { mean: data[25], min: data[26], max: data[27] }
  }
}

/**
 * @param {Array.<Metrics>} metrics
 */
function sortMetrics (metrics) {
  metrics.sort((a, b) => a.date - b.date)
}

/**
 * @param {Array.<Metrics>} metrics
 * @returns {number}
 */
function getBrokenMetricsCount (metrics) {
  let count = 0
  for (let i = 0; i < metrics.length; i++) {
    const nulls = metrics[i].data.filter((value) => value === null)
    if (nulls.length) {
      count += 1
    }
  }
  return count
}

/**
 * @param {Array.<Metrics>} metrics
 * @param {number} numberOfDays
 */
function getMissedMetricsCount (metrics, numberOfDays) {
  return (numberOfDays * 144) - metrics.length
}

/**
 * @returns {Array.<null>}
 */
function createEmptyMetricsData () {
  return Array(28).fill(null)
}

/**
 * @param {Array.<Metrics>} metrics
 * @returns {Array.<number>}
 */
function aggregateMetrics (metrics) {
  if (!metrics.length) {
    return createEmptyMetricsData()
  }

  const aggregated = createEmptyMetricsData().fill(0)
  const counts = createEmptyMetricsData().fill(0)

  for (let { data } of metrics) {
    for (let i = 0; i < aggregated.length; i++) {
      aggregated[i] += data[i] || 0
      if (data[i]) { // => (data[i] !== null && data[i] !== 0)
        counts[i] += 1
      }
    }
  }

  for (let i = 0; i < aggregated.length; i++) {
    if (counts[i] > 0) {
      if (i !== 24) { // => not rain
        aggregated[i] /= counts[i]
      }
    } else {
      aggregated[i] = null
    }
  }

  return aggregated
}

module.exports = {
  createMetricsDatabase,
  parseMetricsData,
  sortMetrics,
  getBrokenMetricsCount,
  getMissedMetricsCount,
  createEmptyMetricsData,
  aggregateMetrics
}

/// ----------------------------------------------------------------------------

/**
 * @private
 * @param {MeteoFile} file
 * @returns {Array.<Array.<string>>}
 */
function _convertFileDataToMatrix (file) {
  /* eslint-disable no-multi-spaces */
  return file.data
    .replace(new RegExp(',', 'g'), '.')
    .split('\r\n')
    .filter(Boolean)
    .map((line) => [
      line.slice(0, 8),     //      time
      line.slice(8, 13),    // 0  - windSpeed.mean
      line.slice(13, 18),   // 1  - windSpeed.min
      line.slice(18, 23),   // 2  - windSpeed.max
      line.slice(23, 27),   // 3  - windDirection.mean
      line.slice(27, 31),   // 4  - windDirection.min
      line.slice(31, 35),   // 5  - windDirection.max
      line.slice(35, 41),   // 6  - outsideTemperature.mean
      line.slice(41, 47),   // 7  - outsideTemperature.min
      line.slice(47, 53),   // 8  - outsideTemperature.max
      line.slice(53, 56),   // 9  - outsideHumidity.mean
      line.slice(56, 59),   // 10 - outsideHumidity.min
      line.slice(59, 62),   // 11 - outsideHumidity.max
      line.slice(62, 68),   // 12 - outsideDewpoint.mean
      line.slice(68, 74),   // 13 - outsideDewpoint.min
      line.slice(74, 80),   // 14 - outsideDewpoint.max
      line.slice(80, 87),   // 15 - pressure.mean
      line.slice(87, 94),   // 16 - pressure.min
      line.slice(94, 101),  // 17 - pressure.max
      line.slice(101, 107), // 18 - insideTemperature.mean
      line.slice(107, 113), // 19 - insideTemperature.min
      line.slice(113, 119), // 20 - insideTemperature.max
      line.slice(119, 122), // 21 - insideHumidity.mean
      line.slice(122, 125), // 22 - insideHumidity.min
      line.slice(125, 128), // 23 - insideHumidity.max
      line.slice(128, 132), // 24 - rain
      line.slice(132, 138), // 25 - windChill.mean
      line.slice(138, 144), // 26 - windChill.min
      line.slice(144, 150)  // 27 - windChill.max
    ].map((cell) => cell.trim()))
}

/**
 * @param {MeteoFile} file
 * @param {Array.<string>} row
 * @returns {Metrics}
 */
function _convertMatrixRowToMetrics (file, row) {
  const { year, month, day } = file
  const [ hours, minutes, seconds ] = row[0].split(':').map(Number)
  const date = moment({ year, month, day, hours, minutes, seconds })
  const data = row.slice(1).map((cell) => cell === '' ? null : Number(cell))
  return {
    date,
    data
  }
}
