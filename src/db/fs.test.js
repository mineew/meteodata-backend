const pt = require('path')
const mockFs = require('mock-fs')
const {
  getFilePaths,
  selectValidPaths,
  selectInvalidPaths,
  filterPaths,
  getYearsFromPaths,
  readFiles,
  selectValidFiles,
  selectInvalidFiles
} = require('./fs')

const fs = {
  '/some/root': {
    '20070311.DAT': 'content 1',
    '20070312.DAT': 'content 2',
    '20080722.DAT': 'content 3',
    '20051101.DAT': 'content 4',
    'qwerty.DAT': 'content 5',
    '200511.DAT': 'content 6',
    '20051101 (2).DAT': 'content 7',
    'file.txt': 'content 8',
    'subdir1': {
      'subdir2': {
        '20170915.DAT': 'content 9',
        '20170916.DAT': 'content 10',
        '20180701.DAT': 'content 11',
        '20051027.DAT': 'content 12',
        'file.png': Buffer.from([1, 2, 3])
      }
    },
    // eslint-disable-next-line no-tabs
    'invalid.DAT': 'con	tent'
  }
}

beforeAll(() => {
  mockFs(fs)
})

afterAll(() => {
  mockFs.restore()
})

describe('getFilePaths', () => {
  it('returns a list of paths to meteo data files', async () => {
    expect.assertions(1)
    const paths = await getFilePaths('/some/root')
    expect(paths).toHaveLength(12)
  })

  it('rejects if a root is missing', async () => {
    expect.assertions(1)
    try {
      await getFilePaths('/some/missing/root')
    } catch (e) {
      expect(e.message).toMatch('no such file or directory')
    }
  })
})

describe('selectValidPaths', () => {
  it('selects only valid paths', async () => {
    expect.assertions(1)
    const paths = await getFilePaths('/some/root')
    const valid = selectValidPaths(paths)
    expect(valid).toHaveLength(9)
  })
})

describe('selectInvalidPaths', () => {
  it('selects only invalid paths', async () => {
    expect.assertions(1)
    const paths = await getFilePaths('/some/root')
    const invalid = selectInvalidPaths(paths)
    expect(invalid).toHaveLength(3)
  })
})

describe('filterPaths', () => {
  let paths = []

  beforeAll(async () => {
    paths = await getFilePaths('/some/root')
    paths = selectValidPaths(paths)
  })

  it('by default, doesn\'t filter anything at all', () => {
    const filtered = filterPaths(paths)
    expect(filtered).toHaveLength(9)
  })

  it('filters paths by years', () => {
    const filtered = filterPaths(paths, { years: [2007, 2005] })
    const filteredNames = filtered.map((path) => pt.basename(path))
    filteredNames.sort()
    expect(filteredNames).toEqual([
      '20051027.DAT',
      '20051101 (2).DAT',
      '20051101.DAT',
      '20070311.DAT',
      '20070312.DAT'
    ])
  })

  it('filters paths by months', () => {
    const filtered = filterPaths(paths, { months: [2, 6] })
    const filteredNames = filtered.map((path) => pt.basename(path))
    filteredNames.sort()
    expect(filteredNames).toEqual([
      '20070311.DAT',
      '20070312.DAT',
      '20080722.DAT',
      '20180701.DAT'
    ])
  })

  it('filters paths by years and months', () => {
    const filtered = filterPaths(paths, { years: [2007, 2005], months: [2, 9] })
    const filteredNames = filtered.map((path) => pt.basename(path))
    filteredNames.sort()
    expect(filteredNames).toEqual([
      '20051027.DAT',
      '20070311.DAT',
      '20070312.DAT'
    ])
  })
})

describe('getYearsFromPaths', () => {
  it('extrcts a list of years from a list of paths', async () => {
    expect.assertions(1)
    const paths = await getFilePaths('/some/root')
    const valid = selectValidPaths(paths)
    expect(getYearsFromPaths(valid)).toEqual([
      2005,
      2007,
      2008,
      2017,
      2018
    ])
  })
})

describe('readFiles', () => {
  let paths = []

  beforeAll(async () => {
    paths = await getFilePaths('/some/root')
    paths = selectValidPaths(paths)
  })

  it('reads meteo files', async () => {
    expect.assertions(1)
    const files = (await readFiles(paths)).slice(-3)
    files.sort((a, b) => a.data > b.data ? 1 : b.data > a.data ? -1 : 0)
    expect(files).toEqual([
      { year: 2017, month: 8, day: 16, data: 'content 10' },
      { year: 2018, month: 6, day: 1, data: 'content 11' },
      { year: 2017, month: 8, day: 15, data: 'content 9' }
    ])
  })

  it('reads meteo files in bulks', async () => {
    expect.assertions(1)
    const bulkSize = 4
    const files = await readFiles(paths, bulkSize)
    expect(files).toHaveLength(9)
  })

  it('provides a callback for bulk loading', async () => {
    expect.assertions(1)
    const bulkSize = 4
    const onNextBulkLoaded = jest.fn()
    await readFiles(paths, bulkSize, onNextBulkLoaded)
    expect(onNextBulkLoaded).toHaveBeenCalledTimes(3)
  })

  it('rejects if there are missing files', async () => {
    expect.assertions(1)
    const wrongPaths = [...paths, '/a/missing/file.DAT']
    try {
      await readFiles(wrongPaths)
    } catch (e) {
      expect(e.message).toMatch('no such file or directory')
    }
  })
})

describe('selectValidFiles', () => {
  it('selects only valid files', async () => {
    expect.assertions(1)
    const paths = await getFilePaths('/some/root')
    const files = await readFiles(paths)
    expect(selectValidFiles(files)).toHaveLength(11)
  })
})

describe('selectInvalidFiles', () => {
  it('selects only invalid files', async () => {
    expect.assertions(1)
    const paths = await getFilePaths('/some/root')
    const files = await readFiles(paths)
    expect(selectInvalidFiles(files)).toHaveLength(1)
  })
})
