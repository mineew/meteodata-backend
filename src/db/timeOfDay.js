const moment = require('moment')
const SunCalc = require('suncalc')

/**
 * @typedef {import('moment').Moment} Moment
 */

/**
 * @typedef {import('./metrics').Metrics} Metrics
 */

/**
 * @param {Array.<Metrics>} metrics
 * @param {number} latitude
 * @param {number} longitude
 * @returns {Array.<Metrics>}
 */
function selectLightTimeMetrics (metrics, latitude, longitude) {
  return metrics.filter((m) => _isLight(m, latitude, longitude))
}

/**
 * @param {Array.<Metrics>} metrics
 * @param {number} [dayStart]
 * @param {number} [dayEnd]
 * @returns {Array.<Metrics>}
 */
function selectDarkTimeMetrics (metrics, latitude, longitude) {
  return metrics.filter((m) => _isDark(m, latitude, longitude))
}

module.exports = {
  selectLightTimeMetrics,
  selectDarkTimeMetrics
}

/// ----------------------------------------------------------------------------

/**
 * @typedef {Object} SunTimes
 * @property {{hours: number, minutes: number, hm: number}} sunrise
 * @property {{hours: number, minutes: number, hm: number}} sunset
 */

/**
 * @private
 * @param {Moment} date
 * @param {number} latitude
 * @param {number} longitude
 * @returns {SunTimes}
 */
function _getSunTimes (date, latitude, longitude) {
  const d = moment({
    year: date.year(),
    month: date.month(),
    day: date.date(),
    hours: 12,
    minutes: 30
  }).toDate()
  const { sunrise, sunset } = SunCalc.getTimes(d, latitude, longitude)
  return {
    sunrise: {
      hours: sunrise.getHours(),
      minutes: sunrise.getMinutes(),
      hm: sunrise.getHours() * 60 + sunrise.getMinutes()
    },
    sunset: {
      hours: sunset.getHours(),
      minutes: sunset.getMinutes(),
      hm: sunset.getHours() * 60 + sunset.getMinutes()
    }
  }
}

/**
 * @private
 * @param {Metrics} metrics
 * @param {number} latitude
 * @param {number} longitude
 * @returns {boolean}
 */
function _isLight (metrics, latitude, longitude) {
  const { sunrise, sunset } = _getSunTimes(metrics.date, latitude, longitude)
  const hm = metrics.date.hours() * 60 + metrics.date.minutes()
  return hm >= sunrise.hm && hm < sunset.hm
}

/**
 * @private
 * @param {Metrics} metrics
 * @param {number} latitude
 * @param {number} longitude
 * @returns {boolean}
 */
function _isDark (metrics, latitude, longitude) {
  // const { sunrise, sunset } = _getSunTimes(metrics.date, latitude, longitude)
  // const hm = metrics.date.hours() * 60 + metrics.date.minutes()
  // return hm < sunrise.hm || hm >= sunset.hm
  return !_isLight(metrics, latitude, longitude)
}
