const moment = require('moment')
const {
  getBrokenMetricsCount,
  getMissedMetricsCount,
  aggregateMetrics
} = require('./metrics')
const {
  selectLightTimeMetrics,
  selectDarkTimeMetrics
} = require('./timeOfDay')

/**
 * @typedef {import('./metrics').Metrics} Metrics
 */

/**
 * @typedef {Object} CalendarData
 * @property {Array.<number>} fullTime
 * @property {Array.<number>} lightTime
 * @property {Array.<number>} darkTime
 * @property {number} totalCount
 * @property {number} missedCount
 * @property {number} brokenCount
 */

/**
 * @typedef {CalendarData & {month:number}} CalendarMonth
 */

/**
 * @typedef {CalendarData & {day:number}} CalendarDay
 */

/**
 * @param {Array.<Metrics>} metrics
 * @param {number} year
 * @param {number} latitude
 * @param {number} longitude
 * @returns {Array.<CalendarMonth>}
 */
function getCalendarYear (metrics, year, latitude, longitude) {
  const yearMetrics = metrics.filter((m) => m.date.year() === year)
  const calendarMonths = []
  for (let i = 0; i < 12; i++) {
    const monthMetrics = yearMetrics.filter((m) => m.date.month() === i)
    if (monthMetrics.length) {
      calendarMonths.push(_createCalendarMonth(
        monthMetrics, year, i, latitude, longitude
      ))
    }
  }
  return calendarMonths
}

/**
 * @param {Array.<Metrics>} metrics
 * @param {number} year
 * @param {number} month
 * @param {number} latitude
 * @param {number} longitude
 * @returns {Array.<CalendarDay>}
 */
function getCalendarMonth (metrics, year, month, latitude, longitude) {
  const monthMetrics = metrics.filter(
    (m) => m.date.year() === year &&
           m.date.month() === month
  )
  const calendarDays = []
  const numberOfDays = _getNumberOfDaysInMonth(year, month)
  for (let i = 1; i <= numberOfDays; i++) {
    const dayMetrics = monthMetrics.filter((m) => m.date.date() === i)
    if (dayMetrics.length) {
      calendarDays.push(_createCalendarDay(
        dayMetrics, i, latitude, longitude
      ))
    }
  }
  return calendarDays
}

module.exports = {
  getCalendarYear,
  getCalendarMonth
}

/// ----------------------------------------------------------------------------

/**
 * @private
 * @param {number} year
 * @param {number} month
 * @returns {number}
 */
function _getNumberOfDaysInMonth (year, month) {
  return moment(`${year}-${month + 1}`, 'YYYY-MM').daysInMonth()
}

/**
 * @private
 * @param {Array.<Metrics>} metrics
 * @param {number} missedCount
 * @param {number} latitude
 * @param {number} longitude
 * @returns {CalendarData}
 */
function _createCalendarData (metrics, missedCount, latitude, longitude) {
  const lightTime = selectLightTimeMetrics(metrics, latitude, longitude)
  const darkTime = selectDarkTimeMetrics(metrics, latitude, longitude)
  return {
    fullTime: aggregateMetrics(metrics),
    lightTime: aggregateMetrics(lightTime),
    darkTime: aggregateMetrics(darkTime),
    totalCount: metrics.length,
    missedCount,
    brokenCount: getBrokenMetricsCount(metrics)
  }
}

/**
 * @private
 * @param {Array.<Metrics>} monthMetrics
 * @param {number} year
 * @param {number} month
 * @param {number} latitude
 * @param {number} longitude
 * @returns {CalendarMonth}
 */
function _createCalendarMonth (monthMetrics, year, month, latitude, longitude) {
  const days = _getNumberOfDaysInMonth(year, month)
  const missedCount = getMissedMetricsCount(monthMetrics, days)
  return {
    month,
    ..._createCalendarData(monthMetrics, missedCount, latitude, longitude)
  }
}

/**
 * @private
 * @param {Array.<Metrics>} dayMetrics
 * @param {number} year
 * @param {number} month
 * @param {number} day
 * @param {number} latitude
 * @param {number} longitude
 * @returns {CalendarDay}
 */
function _createCalendarDay (dayMetrics, day, latitude, longitude) {
  const missedCount = getMissedMetricsCount(dayMetrics, 1)
  return {
    day,
    ..._createCalendarData(dayMetrics, missedCount, latitude, longitude)
  }
}
