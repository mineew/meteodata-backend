const moment = require('moment')
const {
  createEmptyMetricsData
} = require('./metrics')
const {
  getTable
} = require('./table')

const metrics = []

for (let day = 1; day <= 5; day++) {
  for (let hour = 0; hour < 24; hour++) {
    for (let minute = 0; minute <= 50; minute += 10) {
      const dayf = `0${day}`
      const hourf = hour < 10 ? `0${hour}` : hour.toString()
      const minutef = minute < 10 ? `0${minute}` : minute.toString()
      const date = moment(
        `${dayf}.11.2011 ${hourf}:${minutef}`, 'DD.MM.YYYY HH:mm'
      )
      const data = createEmptyMetricsData().fill(day + hour + minute)
      metrics.push({ date, data })
    }
  }
}

describe('getTable', () => {
  it('creates a dataset suitable for table view', () => {
    const start = moment('02.11.2011', 'DD.MM.YYYY').unix()
    const end = moment('03.11.2011', 'DD.MM.YYYY').unix()
    const table = getTable(metrics, start, end)
    expect(table).toHaveLength(144 * 2)
    const days = [...new Set(table.map((r) => r.date))]
    expect(days).toEqual([
      '02.11.2011',
      '03.11.2011'
    ])
    expect(table[7]).toEqual({
      date: '02.11.2011',
      time: '01:10',
      data: createEmptyMetricsData().fill(2 + 1 + 10)
    })
  })

  it('can fill gaps', () => {
    const metricsWithGaps = metrics.filter(
      (m) => m.date.minutes() !== 10 && m.date.minutes() !== 20
    )
    const start = moment('02.11.2011', 'DD.MM.YYYY').unix()
    const end = moment('03.11.2011', 'DD.MM.YYYY').unix()
    const table = getTable(metricsWithGaps, start, end)
    expect(table[6]).toEqual({
      date: '02.11.2011',
      time: '01:00',
      data: createEmptyMetricsData().fill(2 + 1 + 0)
    })
    expect(table[7]).toEqual({
      date: '02.11.2011',
      time: '01:10',
      data: createEmptyMetricsData()
    })
    expect(table[8]).toEqual({
      date: '02.11.2011',
      time: '01:20',
      data: createEmptyMetricsData()
    })
    expect(table[9]).toEqual({
      date: '02.11.2011',
      time: '01:30',
      data: createEmptyMetricsData().fill(2 + 1 + 30)
    })
  })

  it('can group metrics', () => {
    const start = moment('02.11.2011', 'DD.MM.YYYY').unix()
    const end = moment('03.11.2011', 'DD.MM.YYYY').unix()
    const tableWithoutGrouping = getTable(metrics, start, end)
    expect(tableWithoutGrouping).toHaveLength(288)
    const tableWithGrouping1 = getTable(metrics, start, end, 2)
    expect(tableWithGrouping1).toHaveLength(144)
    const tableWithGrouping2 = getTable(metrics, start, end, 5)
    expect(tableWithGrouping2).toHaveLength(58)
  })

  it('can add hours', () => {
    const start = moment('02.11.2011', 'DD.MM.YYYY').unix()
    const end = moment('03.11.2011', 'DD.MM.YYYY').unix()
    const normalTable = getTable(metrics, start, end)
    expect(normalTable[0].time).toBe('00:00')
    const addHoursTable = getTable(metrics, start, end, 1, 2)
    expect(addHoursTable[0].time).toBe('02:00')
  })
})
