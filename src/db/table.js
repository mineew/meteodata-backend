const moment = require('moment')
const chunk = require('lodash.chunk')
const {
  aggregateMetrics,
  createEmptyMetricsData
} = require('./metrics')

/**
 * @typedef {import('moment').Moment} Moment
 */

/**
 * @typedef {import('./metrics').Metrics} Metrics
 */

/**
 * @typedef {Object} TableRow
 * @property {string} date
 * @property {string} time
 * @property {Array.<number|null>} data
 */

/**
 * @param {Array.<Metrics>} metrics
 * @param {number} start
 * @param {number} end
 * @param {number} [grouping]
 * @param {number} [addHours]
 * @returns {Array.<TableRow>}
 */
function getTable (metrics, start, end, grouping = 1, addHours = 0) {
  const startDate = moment.unix(start)
  const endDate = moment.unix(end).add(1, 'days')
  let tableMetrics = metrics.filter(
    (m) => m.date >= startDate && m.date < endDate
  )
  if (addHours) {
    tableMetrics = _addHoursToMetrics(tableMetrics, addHours)
  }
  if (grouping > 1) {
    tableMetrics = _groupMetrics(tableMetrics, grouping)
  }
  const table = []
  for (let i = 0; i < tableMetrics.length - 1; i++) {
    const current = tableMetrics[i]
    const next = tableMetrics[i + 1]
    table.push(
      _createTableRow(current.date, current.data),
      ..._createGaps(current, next, grouping * 10)
    )
  }
  const last = tableMetrics[tableMetrics.length - 1]
  table.push(_createTableRow(last.date, last.data))
  return table
}

module.exports = {
  getTable
}

/// ----------------------------------------------------------------------------

/**
 * @private
 * @param {Array.<Metrics>} metrics
 * @param {number} hours
 */
function _addHoursToMetrics (metrics, hours) {
  return metrics.map((m) => ({
    ...m,
    date: m.date.clone().add(hours, 'hours')
  }))
}

/**
 * @private
 * @param {Array.<Metrics>} metrics
 * @param {number} grouping
 * @returns {Array.<Metrics>}
 */
function _groupMetrics (metrics, grouping) {
  const groups = chunk(metrics, grouping)
  return groups.map((g) => {
    const aggregated = aggregateMetrics(g)
    return { date: g[0].date, data: aggregated }
  })
}

/**
 * @private
 * @param {Moment} date
 * @param {Array.<number|null>} data
 * @returns {TableRow}
 */
function _createTableRow (date, data) {
  return {
    date: date.format('DD.MM.YYYY'),
    time: date.format('HH:mm'),
    data
  }
}

/**
 * @private
 * @param {Metrics} metricsA
 * @param {Metrics} metricsB
 * @returns {number}
 */
function _diffInMinutes (metricsA, metricsB) {
  return metricsB.date.diff(metricsA.date, 'minutes')
}

/**
 * @private
 * @param {Metrics} metricsA
 * @param {Metrics} metricsB
 * @param {number} step
 * @returns {Array.<TableRow>}
 */
function _createGaps (metricsA, metricsB, step) {
  const minutes = _diffInMinutes(metricsA, metricsB)
  const gaps = []
  if (minutes > step) {
    const startGap = metricsA.date.clone()
    for (let i = 0; i < Math.floor(minutes / 10) - 1; i++) {
      gaps.push(_createTableRow(
        startGap.add(10, 'minutes'),
        createEmptyMetricsData()
      ))
    }
  }
  return gaps
}
