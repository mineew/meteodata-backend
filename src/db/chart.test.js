const {
  createEmptyMetricsData
} = require('./metrics')
const calendarModule = require('./calendar')
const tableModule = require('./table')

calendarModule.getCalendarYear = jest.fn(() => [{
  month: 9,
  fullTime: createEmptyMetricsData().fill(9 + 1),
  lightTime: createEmptyMetricsData().fill(9 + 2),
  darkTime: createEmptyMetricsData().fill(9 + 3)
}, {
  month: 10,
  fullTime: createEmptyMetricsData().fill(10 + 1),
  lightTime: createEmptyMetricsData().fill(10 + 2),
  darkTime: createEmptyMetricsData().fill(10 + 3)
}])

calendarModule.getCalendarMonth = jest.fn(() => [{
  day: 1,
  fullTime: createEmptyMetricsData().fill(1 + 1),
  lightTime: createEmptyMetricsData().fill(1 + 2),
  darkTime: createEmptyMetricsData().fill(1 + 3)
}, {
  day: 2,
  fullTime: createEmptyMetricsData().fill(2 + 1),
  lightTime: createEmptyMetricsData().fill(2 + 2),
  darkTime: createEmptyMetricsData().fill(2 + 3)
}])

tableModule.getTable = jest.fn()
tableModule.getTable
  .mockReturnValueOnce([{
    date: '12.01.2019',
    time: '00:10',
    data: createEmptyMetricsData().fill(1)
  }, {
    date: '12.01.2019',
    time: '00:20',
    data: createEmptyMetricsData().fill(2)
  }])
  .mockReturnValueOnce([{
    date: '12.01.2019',
    time: '00:10',
    data: createEmptyMetricsData().fill(3)
  }, {
    date: '13.01.2019',
    time: '00:20',
    data: createEmptyMetricsData().fill(4)
  }])
  .mockReturnValueOnce([{
    date: '12.01.2019',
    time: '00:10',
    data: createEmptyMetricsData().fill(4)
  }, {
    date: '13.02.2019',
    time: '00:20',
    data: createEmptyMetricsData().fill(5)
  }])
  .mockReturnValueOnce([{
    date: '12.01.2019',
    time: '00:10',
    data: createEmptyMetricsData().fill(7)
  }, {
    date: '13.02.2020',
    time: '00:20',
    data: createEmptyMetricsData().fill(8)
  }])
  .mockReturnValue([{
    date: '12.01.2019',
    time: '00:10',
    data: createEmptyMetricsData().fill(9)
  }])

const {
  getChartYear,
  getChartMonth,
  getChartTable
} = require('./chart')

// Архангельск
const arhLat = 64.5
const arhLong = 40.5

describe('getChartYear', () => {
  it('creates a dataset suitable for chart year view', () => {
    const chartYear = getChartYear([], 'pressure', 2019, arhLat, arhLong)
    expect(chartYear.fullTime).toHaveLength(12)
    expect(chartYear.lightTime).toHaveLength(12)
    expect(chartYear.darkTime).toHaveLength(12)
    expect(chartYear.fullTime[9].name).toBe('окт.')
    expect(chartYear.fullTime[9].mean).toBe(10)
    expect(chartYear.fullTime[9].min).toBe(10)
    expect(chartYear.fullTime[9].max).toBe(10)
    expect(calendarModule.getCalendarYear).toHaveBeenCalledTimes(1)
    expect(calendarModule.getCalendarYear).toHaveBeenLastCalledWith(
      [], 2019, arhLat, arhLong
    )
  })

  it('returns null\'s if there is no such metrics', () => {
    const chartYear = getChartYear([], 'unknown', 2019, arhLat, arhLong)
    expect(chartYear.fullTime[0].mean).toBe(null)
    expect(chartYear.fullTime[0].min).toBe(null)
    expect(chartYear.fullTime[0].max).toBe(null)
  })
})

describe('getChartMonth', () => {
  it('creates a dataset suitable for chart month view', () => {
    const chartMonth = getChartMonth([], 'rain', 2019, 1, arhLat, arhLong)
    expect(chartMonth.fullTime).toHaveLength(28)
    expect(chartMonth.lightTime).toHaveLength(28)
    expect(chartMonth.darkTime).toHaveLength(28)
    expect(chartMonth.lightTime[0].name).toBe('01')
    expect(chartMonth.lightTime[0].mean).toBe(3)
    expect(chartMonth.lightTime[0].min).toBe(3)
    expect(chartMonth.lightTime[0].max).toBe(3)
    expect(calendarModule.getCalendarMonth).toHaveBeenCalledTimes(1)
    expect(calendarModule.getCalendarMonth).toHaveBeenLastCalledWith(
      [], 2019, 1, arhLat, arhLong
    )
  })

  it('returns null\'s if there is no such metrics', () => {
    const chartMonth = getChartMonth([], 'unknown', 2019, 1, arhLat, arhLong)
    expect(chartMonth.lightTime[0].mean).toBe(null)
    expect(chartMonth.lightTime[0].min).toBe(null)
    expect(chartMonth.lightTime[0].max).toBe(null)
  })
})

describe('getChartTable', () => {
  it('creates a dataset suitable for chart table view', () => {
    const chartTable = getChartTable([], 'pressure', 0, 0)
    expect(chartTable).toHaveLength(2)
    expect(chartTable[0].name).toBe('00:10')
    expect(chartTable[0].mean).toBe(1)
    expect(chartTable[0].min).toBe(1)
    expect(chartTable[0].max).toBe(1)
    expect(tableModule.getTable).toHaveBeenCalledTimes(1)
    expect(tableModule.getTable).toHaveBeenLastCalledWith(
      [], 0, 0, 1, 0
    )
  })

  it('gives the appropriate names', () => {
    const chartTableDifferentDays = getChartTable([], 'pressure', 0, 0)
    const chartTableDifferentMonths = getChartTable([], 'pressure', 0, 0)
    const chartTableDifferentYears = getChartTable([], 'pressure', 0, 0)
    expect(chartTableDifferentDays[0].name).toBe('12 00:10')
    expect(chartTableDifferentMonths[0].name).toBe('12.01 00:10')
    expect(chartTableDifferentYears[0].name).toBe('12.01.2019 00:10')
  })

  it('returns null\'s if there is no such metrics', () => {
    const chartTable = getChartTable([], 'unknown', 0, 0)
    expect(chartTable[0].mean).toBe(null)
    expect(chartTable[0].min).toBe(null)
    expect(chartTable[0].max).toBe(null)
  })
})
