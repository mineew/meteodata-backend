const pt = require('path')
const fs = require('fs')
const walk = require('klaw')

/**
 * @param {string} root
 * @returns {Promise.<Array.<string>>}
 */
function getFilePaths (root) {
  return _getFilePaths(root, '.DAT')
}

/**
 * @param {Array.<string>} paths
 * @returns {Array.<string>}
 */
function selectValidPaths (paths) {
  return paths.filter((path) => _fileNameIsValid(path))
}

/**
 * @param {Array.<string>} paths
 * @returns {Array.<string>}
 */
function selectInvalidPaths (paths) {
  return paths.filter((path) => !_fileNameIsValid(path))
}

/**
 * @param {Array.<string>} paths
 * @param {{years?:Array.<number>, months?:Array.<number>}} [opts] months start from 0
 * @returns {Array.<string>}
 */
function filterPaths (paths, opts = {}) {
  const { years, months } = opts
  return paths.filter((path) => {
    const { year, month } = _parseFileName(path)
    if (years && !years.includes(year)) {
      return false
    }
    if (months && !months.includes(month)) {
      return false
    }
    return true
  })
}

/**
 * @param {Array.<string>} paths
 * @returns {Array.<number>}
 */
function getYearsFromPaths (paths) {
  const years = []
  paths.map((path) => {
    const { year } = _parseFileName(path)
    if (!years.includes(year)) {
      years.push(year)
    }
  })
  years.sort((a, b) => a - b)
  return years
}

/**
 * @typedef {Object} MeteoFile
 * @property {number} year
 * @property {number} month starts from 0
 * @property {number} day
 * @property {string} data
 */

/**
 * @param {Array.<string>} paths
 * @param {number} [bulkSize]
 * @param {function(Array.<MeteoFile>)} [onNextBulkLoaded]
 * @returns {Promise.<Array.<MeteoFile>>}
 */
function readFiles (paths, bulkSize = 0, onNextBulkLoaded = null) {
  if (!bulkSize) {
    return _readFiles(paths)
  }
  return _readFilesInBulks(paths, bulkSize, onNextBulkLoaded)
}

/**
 * @param {Array.<MeteoFile>} files
 * @returns {Array.<MeteoFile>}
 */
function selectValidFiles (files) {
  return files.filter((f) => _fileIsValid(f))
}

/**
 * @param {Array.<MeteoFile>} files
 * @returns {Array.<MeteoFile>}
 */
function selectInvalidFiles (files) {
  return files.filter((f) => !_fileIsValid(f))
}

module.exports = {
  getFilePaths,
  selectValidPaths,
  selectInvalidPaths,
  filterPaths,
  getYearsFromPaths,
  readFiles,
  selectValidFiles,
  selectInvalidFiles
}

/// ----------------------------------------------------------------------------

/**
 * @private
 * @param {string} root
 * @param {string} ext
 * @returns {Promise.<Array.<string>>}
 */
function _getFilePaths (root, ext) {
  return new Promise((resolve, reject) => {
    const paths = []
    walk(root, { fs })
      .on('data', ({ path, stats }) => {
        const matchExt = pt.extname(path).toLowerCase() === ext.toLowerCase()
        if (stats.isFile() && matchExt) {
          paths.push(path)
        }
      })
      .on('end', () => {
        resolve(paths)
      })
      .on('error', (error) => {
        reject(error)
      })
  })
}

/**
 * @private
 * @param {string} path
 * @returns {{year:string, month:string, day:string}}
 */
function _parseFileName (path) {
  const name = pt.parse(path).name
  return {
    year: Number(name.slice(0, 4)),
    month: Number(name.slice(4, 6)) - 1,
    day: Number(name.slice(6, 8))
  }
}

/**
 * @private
 * @param {string} path
 * @returns {boolean}
 */
function _fileNameIsValid (path) {
  const name = pt.parse(path).name
  const { year, month, day } = _parseFileName(path)
  return (
    name.length >= 8 &&
    Number.isInteger(year) && (year > 0) &&
    Number.isInteger(month) && (month >= 0 && month < 12) &&
    Number.isInteger(day) && (day > 0 && day <= 31)
  )
}

/**
 * @private
 * @param {string} path
 * @returns {Promise.<MeteoFile>}
 */
function _readFile (path) {
  return new Promise((resolve, reject) => {
    fs.readFile(path, 'utf8', (error, data) => {
      if (error) {
        reject(error)
      } else {
        const { year, month, day } = _parseFileName(path)
        resolve({ year, month, day, data })
      }
    })
  })
}

/**
 * @private
 * @param {Array.<string>} paths
 * @returns {Promise.<Array.<MeteoFile>>}
 */
function _readFiles (paths) {
  return Promise.all(paths.map((path) => _readFile(path)))
}

/**
 * @private
 * @param {Array.<string>} paths
 * @param {number} bulkSize
 * @param {function(Array.<MeteoFile>)|null} onNextBulkLoaded
 * @returns {Promise.<Array.<MeteoFile>>}
 */
async function _readFilesInBulks (paths, bulkSize, onNextBulkLoaded) {
  let processed = 0
  const files = []
  while (processed < paths.length) {
    const bulkPaths = paths.slice(processed, processed + bulkSize)
    const bulkFiles = await _readFiles(bulkPaths)
    if (onNextBulkLoaded) {
      onNextBulkLoaded(bulkFiles)
    }
    files.push(...bulkFiles)
    processed += bulkPaths.length
  }
  return files
}

/**
 * @param {MeteoFile} file
 * @returns {boolean}
 */
function _fileIsValid (file) {
  const { data } = file
  return !data.includes('\t')
}
