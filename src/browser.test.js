const browser = require('./browser')

describe('browser package module', () => {
  it('exports as expected', () => {
    const contents = Object.keys(browser)
    contents.sort()
    expect(contents).toEqual([
      'createEmptyMetricsData',
      'parseMetricsData'
    ])
  })
})
