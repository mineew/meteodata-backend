const main = require('./index')

describe('main package module', () => {
  it('exports as expected', () => {
    const contents = Object.keys(main)
    contents.sort()
    expect(contents).toEqual([
      'createApp',
      'createEmptyMetricsData',
      'parseMetricsData'
    ])
  })
})
