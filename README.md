[![pipeline status](https://gitlab.com/mineew/meteodata-backend/badges/master/pipeline.svg)](https://gitlab.com/mineew/meteodata-backend/commits/master)
[![coverage report](https://gitlab.com/mineew/meteodata-backend/badges/master/coverage.svg)](https://gitlab.com/mineew/meteodata-backend/commits/master)

[![JavaScript Style Guide](https://cdn.rawgit.com/standard/standard/master/badge.svg)](https://github.com/standard/standard)

# meteodata-backend

`meteodata-backend` is a helper package built to provide some functionality
to the [MeteoData](https://gitlab.com/mineew/meteodata-gui) application.

It's main purpose is to parse raw `.DAT` files, convert them to Excel format,
perform some calculations and provide a GraphQL server to query meteo data.

## Install and run

To install and run the package in development mode, type the following in your
terminal:

```
~ $ mkdir meteodata-backend && cd meteodata-backend
~ $ git clone https://gitlab.com/mineew/meteodata-backend.git .
~ $ yarn
~ $ yarn start
```

Then navigate to the [http://localhost:3000/graphql](http://localhost:3000/graphql)
in your browser. This will open the GraphiQL (a graphical interactive
in-browser GraphQL IDE) where you can play with the schema.

## All available scripts

| NPM Script     | Description                             |
| -------------- | --------------------------------------- |
| `yarn start`   | runs the package in development mode    |
| `yarn test`    | tests the package                       |
| `yarn cover`   | shows the code coverage                 |
| `yarn lint`    | lints the code                          |
| `yarn clear`   | removes the temporary folders and files |

## Frameworks and tools

* [Express](https://expressjs.com/) - Fast, unopinionated, minimalist web
  framework for Node.js;
* [GraphQL](https://graphql.org/) - A query language for your API;
* [express-graphql](https://github.com/graphql/express-graphql) - Create a
  GraphQL HTTP server with Express;
* [ExcelJS](https://github.com/exceljs/exceljs) - Read, manipulate and write
  spreadsheet data and styles to XLSX and JSON;
* [SunCalc](https://github.com/mourner/suncalc) - a tiny BSD-licensed
  JavaScript library for calculating sun position.

The tests are written with these tools:

* [Jest](https://jestjs.io/) - Delightful JavaScript Testing.

The code style is managed by:

* [ESlint](https://eslint.org/) - The pluggable linting utility for JavaScript
  and JSX;
* [eslint-config-standard](https://github.com/standard/eslint-config-standard) -
  ESLint Config for JavaScript Standard Style.

Other tools worth mentioning:

* [Moment.js](https://momentjs.com/) - Parse, validate, manipulate, and display
  dates and times in JavaScript.
